#!/usr/bin/env python3
# -*- coding: utf-8 -*- 

import os
import shutil
import math
import numpy as np
import sys

def conv_line(line):
    line = line.replace('\n',' ')
    line = line.replace('\r',' ')
    line = line.replace('\t',' ')
    line = line.replace('^M',' ')
    line = line.split(" ")
    while line.count("") > 0:
        line.remove("")
    return line

path = os.getcwd()
#direcs_hkl = filter(lambda x : os.path.isdir(x), os.listdir(path))
direcs_hkl = [x for x in os.listdir(path) if os.path.isdir(path+"/"+x) == True and x.find("_") == True]
#index_re = direcs_hkl.index("Relax")
#del direcs_hkl[index_re]
direcs_hkl.sort()

ld = ""
for hkl in direcs_hkl:
    ld += hkl + "  "
print(ld)

#read s-list
path_slist = path + "/s-list"
flag_s = os.path.exists(path_slist)
if flag_s == False:
    print("s-list does not exist. You should prepare s-list.")
    sys.exit()

f = open(path_slist)
slines = f.readlines()
f.close()

slist = []
for line in slines:
    line = line.replace('\n','')
    line = line.replace("JobName:","")
    line = line.replace("cutoff:","")
    line = line.replace("k-mesh_in_bulk:","")
    line = line.replace("pseudpot:","")
    line = line.replace("Etot_bulk:","")
    line = line.replace("Natom_bulk:","")
    line = line.split(" ")
    while line.count("") > 0:
        line.remove("")
    slist.append(line)

Etot_bulk = float(slist[4][0])
Natom_bulk = float(slist[5][0])
Eatom_bulk = Etot_bulk/Natom_bulk

list_SumEn = []
append_SumEn = list_SumEn.append
for direc in direcs_hkl:
    path_direc = path + "/" + direc
    list_direc = os.listdir(path_direc)
    direcs_in_hkl = [x for x in list_direc if "Relax" in x]
    direcs_in_hkl.sort()

    for relaxdirec in direcs_in_hkl:
        list_En = []
        append_En = list_En.append

        # Write Sum_TotalandSurfaceEnergy.dat
        Direc = str(direc) + " " + str(relaxdirec) + " "
        append_En(str(direc))
        append_En(str(relaxdirec))

        # Definition of path
        path_direc_relax = path_direc + "/" + relaxdirec
        path_calc = path_direc_relax + "/calc"
        path_pos_incalc = path_calc + "/POSCAR"
        path_outcar = path_calc + "/OUTCAR"
        os.chdir(path_direc_relax)
        
        # Read POSCAR
        f_pos = open(path_pos_incalc)
        poslines = f_pos.readlines()
        f_pos.close()

        # Count Atom number
        atomnum = conv_line(poslines[6])
        Natom_surf = 0
        for num in atomnum:
            Natom_surf += int(num)
        
        # Calculate lattice vector
        line_pa  = list(map(float, conv_line(poslines[2])))
        line_pb  = list(map(float, conv_line(poslines[3])))
        line_pc  = list(map(float, conv_line(poslines[4])))
        lc_a_pos = math.sqrt(float(line_pa[0])**2 + float(line_pa[1])**2 + float(line_pa[2])**2)
        lc_b_pos = math.sqrt(float(line_pb[0])**2 + float(line_pb[1])**2 + float(line_pb[2])**2)
        dot_ab = np.dot(np.array(line_pa), np.array(line_pb))
        S = 0.5 * math.sqrt(lc_a_pos**2 * lc_b_pos**2 - dot_ab**2)

        # Read vauto-input file
        path_ilist = path_direc_relax + "/vauto-input"
        if os.path.exists(path_ilist) == True:
            fi = open(path_ilist)
            ilines = fi.readlines()
            fi.close()

        # Make vauto-input list
        clist = [conv_line(line) for line in ilines if len(conv_line(line)) > 0 and [val for val in conv_line(line) if val != "#"][0] in ["M","N","A"] and conv_line(line)[1] != "A"]
        clist = [[x for x in list if x != "#"] for list in clist]
        #print clist

        list_tot = []
        for redirec in clist:
            list = []
            if str(redirec[8]) == "F":
                direcname = "Prec" + str(redirec[0]) + "cut" + str(redirec[1]) + "k" + str(redirec[2]) +"x"+ str(redirec[3])+ "x" +  str(redirec[4]) + "Ed"+ str(redirec[7])
            elif str(redirec[8]) == "T":
                direcname = "Prec" + str(redirec[0]) + "cut" + str(redirec[1]) + "k" + str(redirec[2]) +"x"+ str(redirec[3])+ "x" +  str(redirec[4]) + "Ed"+ str(redirec[7]) + "Fd" + str(redirec[9])
            print(Direc + direcname)
            append_En(direcname)

            path_direc_inrelax = path_direc_relax + "/" + direcname
            path_stdout = path_direc_inrelax + "/std.out"
            path_outcar = path_direc_inrelax + "/OUTCAR"
                
            # Read OUTCAR
            list_toten = []
            flag_out = os.path.exists(path_outcar)
            if flag_out == True:
                f = open(path_outcar)
                outlines = f.readlines()
                f.close()

                num_o = 0
                while num_o < len(outlines):
                    if outlines[num_o].find("FREE ENERGIE OF THE ION-ELECTRON SYSTEM") >= 0:
                        num_o += 4
                        toten = outlines[num_o]
                        toten = toten.replace('\n','')
                        toten = toten.replace('\r','')
                        toten = toten.replace('\t',' ')
                        toten = toten.split(" ")
                        list_toten.append(toten[-1])
                    else:
                        num_o += 1

            # Read std.out
            flag_stdout = os.path.exists(path_stdout)
            if flag_stdout == False:
                print("std.out does not exist!")
                list.append("Nn ") 
                list.append("--------")
                list.append("----")
                append_En("Nn ")
                append_En("--------")
                append_En("----")

            elif flag_stdout == True:
                f_s = open(path_stdout)
                stdlines = f_s.readlines()
                f_s.close()

                num_bad = 0
                num_dav = 0
                for stdline in stdlines:
                    flag_req  = stdline.find("reached required accuracy - stopping structural energy minimisation")
                    flag_wav  = stdline.find("writing wavefunctions")
                    flag_bad1 = stdline.find("VERY BAD NEWS! internal error in subroutine SGRCON")
                    flag_bad2 = stdline.find("VERY BAD NEWS! internal error in subroutine IBZKPT")
                    flag_dav = stdline.find("DAV:   1")
                    if len(list_toten) > 0:
                        Etot = np.round(float(list_toten[-1]), 3)
                        Esurf = np.round((float(Etot) -Eatom_bulk*Natom_surf)/(2*S)*16, 2)

                    if flag_bad1 >= 0 or flag_bad2 >= 0:
                        num_bad += 1
                    if flag_dav >= 0:
                        num_dav += 1
                    
                    if flag_req >= 0 and len(list_toten) > 0:
                        if len(list_toten) <10:
                            num_scf = str(len(list_toten)) + " "
                        elif len(list_toten) >= 10:
                            num_scf = str(len(list_toten))
                        list.append("R"+num_scf) 
                        list.append(Etot)
                        list.append(Esurf)
                        append_En("R"+num_scf)
                        append_En('%03.3f' % Etot)
                        append_En('%02.2f' % Esurf)
                        break

                    elif flag_wav >= 0 and len(list_toten) > 0:
                        if len(list_toten) <10:
                            num_scf = str(len(list_toten)) + " "
                        elif len(list_toten) >= 10:
                            num_scf = str(len(list_toten))
                        list.append("W"+num_scf) 
                        list.append(Etot)
                        list.append(Esurf)
                        append_En("W"+num_scf)
                        append_En('%03.3f' % Etot)
                        append_En('%02.2f' % Esurf)
                        break

                else:
                    if len(list_toten) > 0:
                        if len(list_toten) <10:
                            num_scf = str(len(list_toten)) + " "
                        elif len(list_toten) >= 10:
                            num_scf = str(len(list_toten))
                        list.append("N"+num_scf) 
                        list.append(Etot)
                        list.append(Esurf)
                        append_En("N"+num_scf)
                        append_En('%03.3f' % Etot)
                        append_En('%02.2f' % Esurf)
                    elif len(list_toten) == 0 and num_bad > 0 and num_dav == 0:
                        list.append("IBE")
                        list.append("--------")
                        list.append("----")
                        append_En("IBE")
                        append_En("--------")
                        append_En("----")

                    else:
                        list.append("N0 ")
                        list.append("--------")
                        list.append("----")
                        append_En("N0 ")
                        append_En("--------")
                        append_En("----")
    
                list_tot.append(list)

        ## End: ##
        append_SumEn(list_En)
        path_energy = path_direc_relax + "/TOTEN.dat"
        f = open(path_energy,'w')
        Lf = ""
        for list_e in list_tot:
            for line in list_e:
                Lf += str(line) + "  "
            else:
                Lf += "\n"
        f.write(Lf)
        f.close()

### Make Sum_TotalandSurfaceEnergy.dat ###
path_sumen = path + "/Sum_TotalandSurfaceEnergy.dat"
file_sumen = open(path_sumen, "w")
file_sumen.write("Directory  [R,W,N]  Etot[eV]  Esurf[J/m2]" + "\n")
L = ""
for list_en in list_SumEn:
    for line in list_en:
        L += str(line) + "  "
    else:
        L += "\n"
file_sumen.write(L)

### Make Sum_TotalandSurfaceEnergy_sorted.dat ###
list_SfEn = []
append_SfEn = list_SfEn.append
list_NonVal = []
append_NonVal = list_NonVal.append
list_num_NonVal = []
append_num_NonVal = list_num_NonVal.append

num_SfEn = 0
for cmp in list_SumEn:
    if cmp[-1] == "----":
        append_NonVal(cmp[-1])
        append_num_NonVal(num_SfEn)
    else:
        append_SfEn(float(cmp[-1]))
        #append_num_SfEn(num_SfEn)
    num_SfEn += 1

list_SfEn_argsort = np.array(list_SfEn).argsort()
list_SfEn_sort = sorted(list_SfEn)
list_SfEn_sorted = []
append_SfEn_sorted = list_SfEn_sorted.append
for nonval in list_num_NonVal:
    append_SfEn_sorted(list_SumEn[nonval])
for val in list_SfEn_argsort:
    append_SfEn_sorted(list_SumEn[val])

path_sumen = path + "/Sum_TotalandSurfaceEnergy_sorted.dat"
file_sumen = open(path_sumen, "w")
file_sumen.write("Directory  [R,W,N]  Etot[eV]  Esurf[J/m2]" + "\n")
L = ""
for list_en in list_SfEn_sorted:
    for line in list_en:
        L += str(line) + "  "
    else:
        L += "\n"
file_sumen.write(L)

print("Done!")

