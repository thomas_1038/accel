#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#make .sh file

import sys
import os
import shutil
import subprocess

def conv_line(line):
    line = line.replace('\n',' ')
    line = line.replace('\r',' ')
    line = line.replace('\t',' ')
    line = line.replace('^M',' ')
    line = line.split(" ")
    while line.count("") > 0:
        line.remove("")
    return line

path = os.getcwd()
path_ilist = path + "/vauto-input"
#path_auto = path + "/calcautorelax_n.py"

if os.path.exists(path_ilist) == False:
    print("\n")
    print("####################################################################")
    print("# vauto-input does not exist! You should prepare vauto-input. BYE! #")
    print("####################################################################")
    print("\n")
    sys.exit()    

# Read c-list0
fi = open(path_ilist)
ilines = fi.readlines()
fi.close()

ilist = [conv_line(line) for line in ilines if len(conv_line(line)) > 0]
del ilist[0]
del ilist[-1]

num_t = 0
num_c = 1000
clist = []
for tag in ilist:
    if "Prec" in tag:
        num_c = num_t

    if num_t > num_c:
        clist.append(tag)
    num_t += 1

clist_be = [list for list in clist if list[0] != "#"]
#print clist
#print clist_be
direcname = "Prec" + str(clist_be[-1][0]) + "cut" + str(clist_be[-1][1]) + "k" + str(clist_be[-1][2]) +"x"+ str(clist_be[-1][3])+ "x" +  str(clist_be[-1][4]) + "Ed"+ str(clist_be[-1][7]) 
path_precN = path + "/" + direcname
INCAR = path_precN + "/INCAR"

# Read INCAR in PrecN
f_i = open(INCAR)
incarlines = f_i.readlines()
f_i.close()

# Change CONTCAR to POSCAR
os.chdir(path_precN)
subprocess.call("contpos.py", shell=True)
subprocess.call("eodel.py", shell=True)
os.chdir(path)

# Change lines in INCAR in PrecN
num_i = 0
while num_i < len(incarlines):
    flag_LWAVE = incarlines[num_i].find("LWAVE = ")
    flag_LCHARG = incarlines[num_i].find("LCHARG = ")

    if flag_LWAVE >= 0:
        value = "LWAVE = .TRUE.\n"
        incarlines[num_i] = value
    elif flag_LCHARG >= 0:
        value ="LCHARG = .TRUE.\n"
        incarlines[num_i] = value
    num_i += 1

L_i = ""
for incarline in incarlines:
    L_i += incarline

f = open(INCAR, "w")
f.write(L_i)
f.close()

# Change lines in c-list
for cline in clist:
    if "#" in cline:
        cline.remove("#")
    elif "#" not in cline:
        cline.insert(0, "#")

L_c = ""
for cline in clist:
    for val in cline:
        L_c += str(val) + " " 
    else:
        L_c += "\n"
print(L_c)

# Change lines in calcautorelax.py
L_input = ""
num_i = 0
num_c = 1000
for iline in ilines:
    if iline.find("calc_set:") >= 0:
        L_input += "calc_set: " + direcname + "\n"
        num_i += 1
        continue

    elif iline.find("Prec") >= 0:
        num_c = num_i

    if num_i > num_c:
        L_input += L_c + "\n"
        L_input += ilines[-1]
        break

    L_input += iline
    num_i += 1

fi = open(path_ilist,"w")
fi.write(L_input)
fi.close()

print("\nDone!\n")



