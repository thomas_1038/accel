#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import os, shutil
import subprocess as sub
import addmol.helper as hl
import numpy as np
import make.mkkp as mkkp
import mkdovasp as mkdv

class Calc_bulk:
    def get_bestkp(self, path_bulk):
        thr_en = 0.05; thr_dens = 27
        path_cutkp1p = path_bulk + "/cutkp1p"
        path_sum_totenandlatcon = path_cutkp1p + "/Sum_TOTENandLATCON.dat"
        path_calc = path_cutkp1p + "/calc"
        path_kplist = path_calc + "/kplist"

        with open(path_sum_totenandlatcon) as fs: sellines = fs.readlines()
        sellines = [hl.conv_line(x) for x in sellines][1:]
        sel_list = np.array(sellines).T
        cutoffs = sel_list[0]
        enperatoms = [float(x) for x in sel_list[4]]
        diff_en = [abs(x - enperatoms[-1]) for x in enperatoms]

        with open(path_kplist) as fk: kplines = fk.readlines()
        kp_list = [hl.conv_line(x)[:3] for x in kplines][1:]
        kp_list = [[int(i) for i in l] for l in kp_list]
        kpdens_list = [hl.conv_line(x)[3:] for x in kplines][1:]
        kpdens_list = [[int(i) for i in l] for l in kpdens_list]

        for i, en in enumerate(diff_en):
            if en <= thr_en and kpdens_list[i][0] >= thr_dens and kpdens_list[i][1] >= thr_dens and kpdens_list[i][2] >= thr_dens:
                cutoff_kp = [int(cutoffs[i]), kp_list[i][0], kp_list[i][1], kp_list[i][2]]
                kp_dens = kpdens_list[i]
                break

        return cutoff_kp, kp_dens

    def make_relax_specific_kp(self, path_bulk):
        nowpath = os.getcwd()
        cutoff_kp = self.get_bestkp(path_bulk)[0]
        cutoff = cutoff_kp[0]; k1 = cutoff_kp[1]; k2 = cutoff_kp[2]; k3 = cutoff_kp[3]

        dirname = "relax_cut" + str(cutoff) + "k" + str(k1) + "x" + str(k2) + "x" + str(k3)
        path_cutkp1p = path_bulk + "/cutkp1p"
        path_calc = path_cutkp1p + "/calc"
        path_inc = path_calc + "/INCAR"
        path_pot = path_calc + "/POTCAR"
        path_pos = path_calc + "/POSCAR"
        path_dir = path_bulk + "/" + dirname
        if not os.path.exists(path_dir): os.mkdir(path_dir)
        shutil.copy(path_pos, path_dir); shutil.copy(path_pot, path_dir)
        mkkp.mk_kp(path_dir, k1, k2 ,k3, "m")

        with open(path_inc) as fi: incarlines = fi.readlines()
        L = ""
        for l in incarlines:
            if   "PREC =" in l and "SYMPREC" not in l : L += "PREC = A\n"
            elif "ENCUT = " in l: L += "ENCUT = " + str(cutoff) + "\n"
            elif "EDIFF =" in l: L += "EDIFF = 1.0e-6\n"
            elif "EDIFFG =" in l: L += "EDIFFG = -1.0e-2\n"
            elif "IBRION =" in l: L += "IBRION = 2\n"
            elif "NSW =" in l: L += "NSW = 1000\n"
            else: L += l
        with open(path_dir + "/INCAR", "w") as fi: fi.write(L)

        path_job = path_dir + "/job.sh"
        Ljob = mkdv.make_dovasp(1, "relax")
        with open(path_job, "w") as fj: fj.write(Ljob)
        os.chmod(path_job, 0o755)
        os.chdir(path_dir); sub.call("qsub job.sh", shell=True)
        os.chdir(nowpath)

if __name__ == "__main__":
    cb = Calc_bulk()
    cb.make_relax_specific_kp(os.getcwd())
    print("DONE!")

# END: script
