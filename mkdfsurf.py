#!/usr/bin/env python3
# -*- coding: utf-8 -*-  
#2017/10/27 version 1.0

import os
from stat import *
import shutil
import sys
import math
import subprocess as sub
import re

def conv_line(line):
    line = line.replace('\n',' ')
    line = line.replace('\r',' ')
    line = line.replace('\t',' ')
    line = line.replace('^M',' ')
    line = line.split(" ")
    while line.count("") > 0:
        line.remove("")
    return line

######## INPUT ZONE #######
path_calcautorun = "/home/nakao/script/nakao_script/calc/calcauto/mkdfmr.py"
path_ilist = "/home/4/17D20121/script/auto/vauto-input-forsurf"
autoscript = "mkdfmr.py"
vasp_ver = "4"
tolerance_kdiff = 0.5
######## INPUT ZONE #######                                                                                                                      

# path
path = os.getcwd()
path_relax = path + "/Relax"
path_calc = path + "/Relax/calc"

# read input
fi = open(path_ilist)
ilines = fi.readlines()
fi.close()

# read s-list
path_slist = path + "/s-list"
flag_s = os.path.exists(path_slist)
if flag_s == False:
    print("\n")
    print("####################  WARNING! ########################")
    print("#                                                     #")
    print("#  s-list does not exist. You should prepare s-list.  #")
    print("#                                                     #")
    print("#######################################################")
    print("\n")

    sys.exit()

f = open(path_slist)
slines = f.readlines()
f.close()
    
slist = []
for line in slines:
    line = line.replace('\n','')
    line = line.replace("JobName:","")
    line = line.replace("cutoff:","")
    line = line.replace("k-mesh_in_bulk:","")
    line = line.replace("pseudpot:","")
    line = line.split(" ")
    while line.count("") > 0:
        line.remove("")
    slist.append(line)

#read POSCAR
path_pc = path + "/POSCAR"
flag_pc = os.path.exists(path_pc)
if flag_pc == False:
    print("\n")
    print("####################  WARNING! ########################")
    print("#                                                     #")
    print("#  POSCAR does not exist. You should prepare POSCAR!  #")
    print("#                                                     #")
    print("#######################################################")
    print("\n")
    sys.exit()

fp = open(path_pc)
poslines= fp.readlines()
fp.close()

### caculate the lattice vector of POSCAR ###
line_pa = conv_line(poslines[2])
line_pb = conv_line(poslines[3])
line_pc = conv_line(poslines[4])

lc_a_pos = math.sqrt(float(line_pa[0])**2 + float(line_pa[1])**2 + float(line_pa[2])**2)
lc_b_pos = math.sqrt(float(line_pb[0])**2 + float(line_pb[1])**2 + float(line_pb[2])**2)
lc_c_pos = math.sqrt(float(line_pc[0])**2 + float(line_pc[1])**2 + float(line_pc[2])**2)

kx = float(slist[2][0])
ky = float(slist[2][1])
kz = float(slist[2][2])

lkx = round(float(1/lc_a_pos/kx)*100, 3)
lky = round(float(1/lc_b_pos/ky)*100, 3)
lkz = round(float(1/lc_c_pos/kz)*100, 3)
lk = "lkx: " + str(lkx) + "  " + "lky: " + str(lky) + "  " + "lkz: " + str(lkz) + "\n"
print(lk)
### Fin ###                  

#make directories
if os.path.exists(path_relax) == True:
    
    # print vauto-input
    path_input = path_relax + "/vauto-input"
    fi = open(path_input)
    ilines_re = fi.readlines()
    fi.close()
    
    Lva = ""
    for iline in ilines_re:
        Lva += iline

    # print INCAR
    path_incar =  path_calc + "/INCAR"
    f_in = open(path_incar)
    incarlines = f_in.readlines()
    f_in.close()
    
    incarlines_forpr = [x for x in incarlines if "!" not in x and x != "\n"]
    Lin = ""
    for line in incarlines_forpr:
        Lin += line 
    
    print(Lva + "\n")
    print(Lin + "\n")

    Q2 = input("If you want to change INCAR, please input [q or Q]: ")
    if Q2 in ["q", "Q"]:
        print("Exit!")
        sys.exit()

elif os.path.exists(path_relax) == False:
    os.mkdir(path_relax)
    os.mkdir(path_calc)

    ## make autorunprogram
    fi = open(path_ilist)
    ilines = fi.readlines()
    fi.close()

    ilist = [conv_line(line) for line in ilines if len(conv_line(line)) > 0]
    del ilist[0]
    del ilist[-1]

    num_t = 0
    num_c = 10000
    clist = []
    for tag in ilist:
        if "LT:" in tag:
            LT = float(tag[1])
        elif "calc_set:" in tag:
            calcset = str(tag[1])
        elif "job_name:" in tag:
            job_name = str(tag[1])
        elif "kp_method:" in tag:
            kp_method = str(tag[1])
        elif "cp_wc:" in tag:
            cp_wc = str(tag[1])
        elif "vasp_type:" in tag:
            vasp_type = str(tag[1])
        elif "node_type:" in tag:
            node_type = str(tag[1])
        elif "num_node:" in tag:
            num_node = str(tag[1])
        elif "process:" in tag:
            process = str(tag[1])
        elif "NPAR:" in tag:
            NPAR = str(tag[1])
        elif "KPAR:" in tag:
            KPAR = str(tag[1])
        elif "job_time:" in tag:
            job_time = str(tag[1])
        elif "Prec" in tag:
            num_c = num_t

        if num_t > num_c and tag[0] != "#":
            clist.append(tag)
            num_t += 1

    print("job_name: " + job_name)
    print("vasp_type = " + vasp_type)
    print("node_type = " + node_type)
    print("num_node = " + num_node)
    print("process = " + process)
    print("NPAR = " + NPAR)
    print("KPAR = " + KPAR)
    print("job_time = " + job_time)
    print("kp_method = " + kp_method + "\n")

    Q_job = input("Please input the job name or default, d: ")
    if Q_job in ["d"]:
        pass
    else:
        job_name = Q_job

    Q_chg = input("Do you want to change above parameters? [y/N]: ")
    if Q_chg in ["n","N"]:
        pass
    elif Q_chg in ["y","Y"]:
        Q_vasptype = input("Please input the type of vasp, [std,s/ ncl,n / gam,gm / gpu,gp / default,d]: ")
        if Q_vasptype in ["d"]:
            pass
        elif Q_vasptype in ["s","std"]:
            vasptype = "vasp_std"
        elif Q_vasptype in ["n","ncl"]:
            vasptype = "vasp_ncl"
        elif Q_vasptype in ["gm","gamma"]:
            vasptype = "vasp_gamma"
        elif Q_vasptype in ["gp","gpu"]:
            vasptype = "vasp_gpu"

        Q_nodetype = input("Please input the type of node, [default, d / f_node, fn / h_node, hn / q_node, qn / s_core, sc / q_core, qc / s_gpu, g ]: ")
        if Q_nodetype in ["d"]:
            pass
        elif Q_nodetype in ["fn", "f_node"]:
            nodetype = "f_node"
        elif Q_nodetype in["hn", "h_node"]:
            nodetype = "h_node"
        elif Q_nodetype in["qn", "q_node"]:
            nodetype = "q_node"
        elif Q_nodetype in["sc", "s_core"]:
            nodetype = "s_core"
        elif Q_nodetype in["qc", "q_core"]:
            nodetype = "q_core"
        elif Q_nodetype in["g", "s_gpu"]:
            nodetype = "s_gpu"

        Q_numnode = input("Please input the number of node you want to use. e.g., 1 or defalt, d: ")
        if Q_numnode in ["d"]:
            pass
        else:
            numnode = Q_numnode

        Q_process = input("Please input the number of process you want to use. e.g., 3 or default, d: ")
        if Q_process in ["d"]:
            pass
        else:
            process = Q_process
            
        Q_NPAR = input("Please input NPAR you want to use. e.g., 4 or default, d: ")
        if Q_NPAR in ["d"]:
            pass
        else:
            NPAR = Q_NPAR
            
        Q_KPAR = input("Please input KPAR you want to use. e.g., 1 or default, d: ")
        if Q_KPAR in ["d"]:
            pass
        else:
            KPAR = Q_KPAR
        
        Q_time = input("Please input the wall time you want to use. e.g., 1:00:00 or default, d: ")
        if Q_time in ["d"]:
            pass
        else:
            job_time = Q_time

        Q_kpmethod = input("Please input k-points method, Gamma center or Monkhorst-Pack, [g/m] or default, d: ")
        if Q_kpmethod in ["d"]:
            pass
        elif Q_kpmethod in ["g"]:
            kp_method = "G"
        elif Q_kpmethod in ["m"]:
            kp_method = "M"
    
    ilines_re = []
    for iline in ilines:
        if iline.find("job_name:") >= 0:
            iline = "job_name: " + job_name + "\n"
        elif iline.find("kp_method:") >= 0:
            iline = "kp_method: " + kp_method + "\n"
        elif iline.find("vasp_type:") >= 0:
            iline = "vasp_type: " + vasp_type + "\n"
        elif iline.find("node_type:") >= 0:
            iline = "node_type: " + node_type + "\n"
        elif iline.find("num_node:") >= 0:
            iline = "num_node: "+ num_node + "\n"
        elif iline.find("process:") >= 0:
            iline = "process: " + process + "\n"
        elif iline.find("NPAR:") >= 0:
            iline = "NPAR: " + NPAR + "\n"
        elif iline.find("KPAR:") >= 0:
            iline = "KPAR: " + KPAR + "\n"
        elif iline.find("job_time:") >= 0:
            iline = "job_time: " + job_time + "\n"
        ilines_re.append(iline)

    path_input = path_relax + "/vauto-input"  
    fi = open(path_input,"w")
    Lva = ""
    for iline in ilines_re:
        fi.write(iline)
        Lva += iline 
    fi.close()
    
    ## make POTCAR
    os.chdir(path_calc)
    pot = ""
    for p in slist[3]:
        pot += str(p) + " "
    mkpot = "mkpot.py " + pot #+ " " + vasp_ver
    sub.call(mkpot,shell=True)

    ## make INCAR
    metal_or_else = input("Metal or else? Please input [m or e]: ")
    if metal_or_else == "m": 
        sub.call("mkincar.py -sm", shell=True)
    else:
        sub.call("mkincar.py -se", shell=True)
    path_incar = path_calc + "/INCAR"

    ## read INCAR
    f_in = open(path_incar)
    incarlines = f_in.readlines()
    f_in.close()

    # print vauto-input & INCAR
    incarlines_forpr = [x for x in incarlines if "!" not in x and x != "\n"]
    Lin = ""
    for line in incarlines_forpr:
        Lin += line

    print(Lva +"\n")
    print(Lin + "\n")

    Q2 = input("If you want to change INCAR, please input [q or Q]: ")
    if Q2 in ["q", "Q"]:
        print("Exit!")
        sys.exit()

### make direcs ###
p1 = r"[0-9]_[0-9]_[0-9]"
p2 = r"-[0-9]_[0-9]_[0-9]"
p3 = r"[0-9]_-[0-9]_[0-9]"
p4 = r"[0-9]_[0-9]_-[0-9]"
p5 = r"-[0-9]_-[0-9]_[0-9]"
p6 = r"[0-9]_-[0-9]_-[0-9]"
p7 = r"-[0-9]_[0-9]_-[0-9]"
p8 = r"-[0-9]_-[0-9]_-[0-9]"
dirs = [x for x in os.listdir(path) if os.path.isdir(path+"/"+x) == True and re.match(p1,x) or re.match(p2,x) or re.match(p3,x) or re.match(p4,x) or re.match(p5,x) or re.match(p6,x) or re.match(p7,x) or re.match(p8,x)]
dirs.sort()

L_dirsf = ""
for dir_surf in dirs:
    L_dirsf += str(dir_surf) + "  "

print(L_dirsf)
### Fin ###

#make c-list-sum #start
path_sumclist = path + "/c-list-sum"
fs = open(path_sumclist, "w") 

### make RealxN in h_k_l directories ###
for direc in dirs:
    print(direc)
    
    #get the name of directory
    path_dir = path + "/" + direc
    poscars = os.listdir(path_dir)

    i = 1
    for pos in poscars:
        Ls = str(direc) + "/Relax" + str(i) + "\n"
        fs.write(Ls)

        path_relaxindir = path_dir + "/Relax" + str(i)
        path_calcindir = path_relaxindir + "/calc"
        shutil.copytree(path_relax, path_relaxindir)

        #copy POSCARs to calc in Relaxs
        path_pos_bf = path_dir + "/" + pos
        path_pos_af = path_calcindir + "/POSCAR" 
        shutil.move(path_pos_bf, path_pos_af)
        
        #make kplist
        os.chdir(path_calcindir)
        sub.call("mkkplist.py",shell=True)
        sub.call("calclatvec.py p",shell=True)

        #read LatticeConstant
        path_lc = path_calcindir + "/LatticeConstant_POSCAR"
        if os.path.exists(path_lc) == False:
            print("LatticeConstant_POSCAR does not exsit. Yoy should prepare LatticeConstant_POSCAR!")
            sys.exit()

        f = open(path_lc)
        lclines = f.readlines()
        f.close()
        lclist=[conv_line(x) for x in lclines]
        del lclist[0]
        #print lclist

        for line in lclines:
            fs.write(line)
        fs.write("\n")

        # read kplist
        path_kplist = path_calcindir + "/kplist"
        if os.path.exists(path_kplist) == False:
            print("\n")
            print("#######################################################")
            print("#  kplist does not exsit. Yoy should prepare kplist!  #")
            print("#######################################################")
            print("\n")
            sys.exit()
        
        f = open(path_kplist)
        kplines = f.readlines()
        f.close()
        kplist=[conv_line(x) for x in kplines]
        del kplist[0]
        #print kplist

        # make diff
        k = 0
        list_lkdiff = []
        list_kpindex = []
        for kp in kplist:
            lkx_kplist = float(kplist[k][3])
            lky_kplist = float(kplist[k][4])
            #lkz_kplist = float(kplist[k][5])
            diffx = lkx_kplist - lkx
            diffy = lky_kplist - lky
            #diffz = abs(lkz_kplist - lkz)

            print("diffx: " + str(diffx) + "  " + "diffy: " + str(diffy))
            if  diffx < tolerance_kdiff:
                list_lkdiff.append(diffx)
                #index = kplist.index(kp)
                list_kpindex.append(kplist.index(kp))

            elif diffy < tolerance_kdiff:
                list_lkdiff.append(diffy)
                #index = kplist.index(kp)
                list_kpindex.append(kplist.index(kp))
            k += 1
        
        # define k-points
        min_index_kp = list_kpindex[0]
        kx_n = kplist[min_index_kp][0]
        ky_n = kplist[min_index_kp][1]
        kx_n_low = int(round(float(kx_n)/2))
        ky_n_low = int(round(float(ky_n)/2))

        # rewrite vauto-input
        cutoff = slist[1][0]
        cutoff_low = int(float(cutoff) -50)
        kp_low = str(kx_n_low) + " " + str(ky_n_low) + " 1"
        kp =str(kx_n) + " " + str(ky_n) + " 1"

        ilines_new = []
        append_i = ilines_new.append
        num = 0
        while num < len(ilines_re):
            if ilines_re[num].find("job_name:") >= 0:
                value = ilines_re[num].replace("\n","") + "-" + str(direc) + "n" + str(i) + "\n"
                #value = "job_name: " + str(slist[0][0]) + "-" + str(direc) + "n" + str(i) + "\n"
                append_i(value)
            elif "M" in ilines_re[num] and "450" in ilines_re[num]:
                value = "M    "   + str(cutoff_low) + "  " + str(kp_low) + "   " + "2      2      1e-4  F  No \n"
                append_i(value)
            elif "N" in ilines_re[num] and "500" in ilines_re[num]:
                value = "N    "   + str(cutoff)     + "  " + str(kp)     + "   " + "2      4      1e-5  F  No \n"
                append_i(value)
            elif "# A" in ilines_re[num] and "500" in ilines_re[num] and "1e-5" in ilines_re[num]: 
                value = "# A    " + str(cutoff)     + "  " + str(kp)     + "   " + "2      6      1e-5  T  1e-2 \n"
                append_i(value)
            elif "# A" in ilines_re[num] and "500" in ilines_re[num] and "1e-6" in ilines_re[num]: 
                value = "# A    " + str(cutoff)     + "  " + str(kp)     + "   " + "2      6      1e-6  T  1e-2 \n"
                append_i(value)
            else:
                append_i(ilines_re[num])
            num += 1

        # make vauto-input
        path_input_in_re = path_relaxindir + "/vauto-input"
        f = open(path_input_in_re, "w")
        for line in ilines_new:
            f.write(line)
        f.close()

        # line for sumlist
        L = "Prec Ecut kpoints IBRION NELMIN EDIFF EDIFFG \n"
        L += "M    "   + str(cutoff_low) + "  " + str(kp_low) + "   " + "2      2      1e-4  F  No \n"
        L += "N    "   + str(cutoff)     + "  " + str(kp)     + "   " + "2      4      1e-5  F  No \n"
        L += "# A    " + str(cutoff)     + "  " + str(kp)     + "   " + "2      6      1e-5  T  1e-2 \n"
        L += "# A    " + str(cutoff)     + "  " + str(kp)     + "   " + "2      6      1e-6  T  1e-2 \n\n"
        fs.write(L)
        i += 1    
        
        os.chdir(path_relaxindir)
        sub.call("mkdfmr.py",shell=True)
        #qsub
        #os.chdir(path_relaxindir)
        #autorun = "nohup ./mkdfmr.py > log.dat &"
        #os.system(autorun)
### Fin ###

fs.close()

print("\n")
print("###################################################")
print("# ALL directories for surf. calc. have been made! #")
print("###################################################")
print("\n")
