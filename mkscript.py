#!/usr/bin/env python3
# -*- coding: utf-8 -*-     

import os
import shutil

path = os.getcwd()

#calculation of auto-relaxation
path_calcautorun = "/home/nakao/script/nakao_script/calc/calcauto/calcautorelax_n.py"
path_ilist = "/home/nakao/script/nakao_script/calc/calcauto/vauto-input"

#cutoff and k-points
path_calccutkp = "/home/nakao/script/nakao_script/calc/calccutkp.py"
path_cutoff = "/home/nakao/script/nakao_script/calc/cutoff"

#surface calculation
path_surfcalc = "/home/nakao/script/nakao_script/calc/surfcalc.py"
path_slist = "/home/nakao/script/nakao_script/calc/s-list"

choice = input("Please input the script name. [cutoff,c /autorelax,a /surfcalc,s]: ")
if choice in ['c','cutoff']:
    shutil.copy(path_calccutkp,path)
    print("calccutkp.py has been made!")
    
    Qc = input("Do you want cutoff? [y/N]: ")
    if Qc in ["y", "yes"]:
        shutil.copy(path_cutoff, path)
        print("cutoff has been made!")

elif choice in ['a', 'autorelax']:
    shutil.copy(path_calcautorun,path)
    print("calcautorelax.py has been made!")
    
    Qcl = input("Do you want vauto-input? [y/N]: ")
    if Qcl in ["y", "yes"]:
        shutil.copy(path_ilist,path)
        print("vauto-input has been made!")


elif choice in ["s", "surfcalc"]:
    shutil.copy(path_surfcalc, path)
    print("surfcalc.py has been made!")
    
    Qs = input("Do you want s-list? [y/N]: ")
    if Qs in ["y", "yes"]:
        shutil.copy(path_slist, path)
        print("s-list has been made!")
