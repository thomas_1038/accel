#!/usr/bin/env python3
# -*- coding: utf-8 -*-  

import os
import sys
import shutil
import threading
import glob
import subprocess
import numpy as np
from scipy import optimize
import sympy as sy
import unittest

def conv_line(line):
    line = line.replace('\n',' ')
    line = line.replace('\r',' ')
    line = line.replace('\t',' ')
    line = line.replace('^M',' ')
    line = line.split(" ")
    while line.count("") > 0:
        line.remove("")
    return line

path = os.getcwd()

T_ini_dC = 200.0
T_fin_dC = 400.0

# Physical constants
kB_J = 1.38064852e-23 #J/K
kB = 8.617330e-05 #eV/K
h_J = 6.62607004e-34 #Js
NA = 6.02E+23 #atom/mol
c = 3.00e+8 #m/s
SS = 0.00962143866705514

# foerward activation energy (EaNf) and diffrence of entalpy (dHN)
Ea1f = 0.0 
Ea2f = 0.27
Ea3f = 0.74
Ea4f = 0.90
Ea5f = 1.22
Ea6f = 0.0
Ea7f = 0.0
Ea8f = 0.44

dH1 = -0.85
dH2 = -1.98086
dH3 = -0.33747 
dH4 = -0.08606 
dH5 = -0.00835 
dH6 = -0.93000 
dH7 = -0.409022
dH8 = -0.117232

Ea1r = Ea1f - dH1
Ea2r = Ea2f - dH2
Ea3r = Ea3f - dH3
Ea4r = Ea4f - dH4
Ea5r = Ea5f - dH5
Ea6r = Ea6f - dH6
Ea7r = Ea7f - dH7
Ea8r = Ea8f - dH8

# vibration energy
hw_N2 = [272.0]
hw_H2 = [5.4557909e+2]
hw_NH3 = []
hw1_fin = [96.475349, 73.942202, 65.302869, 61.067884, 44.026582, 35.500963]
hw2_ini = [96.475349, 73.942202, 65.302869, 61.067884, 44.026582, 35.500963]
hw2_ts =  [79.683305, 72.591623, 59.727744, 46.510172, 29.936295]
hw2_fin = [76.201010, 75.999412, 66.257078, 42.117524, 27.663460, 16.421618]
hw3_ini = [187.153221, 122.005664, 80.694963, 59.113849, 38.841406, 33.747483]
hw3_ts =  [136.701139, 72.864392, 56.377189, 44.258495, 29.936295]
hw3_fin = [408.312565, 107.4459, 80.669267, 62.086941, 51.712956, 27.873004]
hw4_ini = [416.471497, 223.392893, 120.435952, 109.246682, 81.123541, 56.50311, 52.957841, 28.721674, 69.164959]
hw4_ts =  [410.379776, 136.053678, 99.12082, 67.284006, 62.744074, 51.617197, 24.277838, 43.960658]
hw4_fin = [424.831835, 413.449489, 182.776946, 94.989945, 90.301448, 51.938298, 46.626459, 42.593783, 21.717709]
hw5_ini = [433.609005, 422.143931, 186.282995, 153.906922, 127.31795, 91.366273, 87.123714, 80.991111, 63.218707, 55.429998, 38.627425, 19.527]
hw5_ts =  [433.562747, 422.196007, 220.664636, 186.2052, 134.241198, 115.075881, 70.66471, 61.341876, 44.76792, 29.129059, 18.35991]
hw5_fin = [431.656018, 424.996763, 408.460103, 196.235943, 194.978018, 140.550127, 74.41044, 73.48752, 43.973516, 16.528835, 15.915979, 13.300671]
hw6_ini = [431.656018, 424.996763, 408.460103, 196.235943, 194.978018, 140.550127, 74.41044, 73.48752, 43.973516, 16.528835, 15.915979, 13.300671]
hw7_fin = [165.237104, 112.168309, 44.685853]
hw8_ini = [230.299091, 38.225535, 45.166999]
hw8_ts =  [198.105811, 27.593346]
hw8_fin = [151.755987, 78.968715, 58.96792]

# partial pressure
a_pp = 2.75694e-6
b_pp = -3.28266e-3
c_pp = 1.24425
d_pp = -1.45681e+2

def pp(T_K):
    v_NH3 = a_pp * T_K**3 + b_pp * T_K**2 + c_pp * T_K + d_pp
    p_N2 = float((900.0-0.25*v_NH3)/3600.0*100000.0*SS)
    p_H2 = float((2700.0-0.75*v_NH3)/3600.0*100000.0*SS)
    p_NH3 = float(v_NH3/3600.0*100000.0)
    return [p_N2, p_H2, p_NH3]

# partition function of transformation
M_N = 14.0067
M_H = 1.008
M_NH3 = M_N + 3.0 * M_H
W_N2 = 2.0 * M_N/NA * 1.0e-3
W_H2 = 2.0 * M_H/NA * 1.0e-3
W_NH3 = M_NH3/NA * 1.0e-3
def pf_trans(W,T_K):
    pf_trans = kB_J * T_K * ((2.0*np.pi*W*kB_J*T_K)**1.5)/h_J**3
    return pf_trans

# partition function of rotation
Erot_N = 2.4798e-4
Erot_H = 7.5514e-3 

def pf_rot_x2(Erot,T_K):
    pf_rot = kB*T_K/Erot/2.0
    return pf_rot

def pf_rot_nh3(T_K):
    A_nh3 = 9.969488e+2 
    C_nh3 = 6.339147e+2
    pf_rot_nh3 = np.sqrt(np.pi/(A_nh3*A_nh3*C_nh3))*(((kB_J*T_K)/(h_J*c))**1.5)/3.0 
    return pf_rot_nh3

#partition function of vibration
def pf_vib(hw_list,T_K):
    pf_vib_tot = 1.0
    for hw in hw_list:
        pf_vib = 1.0/(1.0-np.exp(-1.0*(float(hw)/1000.0)/(kB*T_K)))
        pf_vib_tot = pf_vib_tot*pf_vib
    return pf_vib_tot

# rate constant and equilibrium constant
def mc_ec(T_K):
    kBTh = kB_J*T_K/h_J

    # 1
    q1_ini = pf_trans(W_N2,T_K)*pf_rot_x2(Erot_N,T_K)*pf_vib(hw_N2,T_K) 
    q1_fin = pf_vib(hw1_fin,T_K)
    K1 = q1_fin/q1_ini*np.exp(-1.0*dH1/(kB*T_K))
    
    # 2
    q2_ini = pf_vib(hw2_ini,T_K)
    q2_ts =  pf_vib(hw2_ts,T_K)
    q2_fin = pf_vib(hw2_fin,T_K)
    K2 =  q2_fin/q2_ini*np.exp(-1.0*dH2/(kB*T_K))
    k2f = kBTh*q2_ts/q2_ini*np.exp(-1.0*Ea2f/(kB*T_K))
    k2r = kBTh*q2_ts/q2_fin*np.exp(-1.0*Ea2r/(kB*T_K))

    # 3
    q3_ini = pf_vib(hw3_ini,T_K)
    q3_ts =  pf_vib(hw3_ts,T_K)
    q3_fin = pf_vib(hw3_fin,T_K)
    K3 =  q3_fin/q3_ini*np.exp(-1*dH3/(kB*T_K))
    k3f = kBTh*q3_ts/q3_ini*np.exp(-1.0*Ea3f/(kB*T_K))
    k3r = kBTh*q3_ts/q3_fin*np.exp(-1.0*Ea3r/(kB*T_K))

    # 4
    q4_ini = pf_vib(hw4_ini,T_K)
    q4_ts =  pf_vib(hw4_ts,T_K)
    q4_fin = pf_vib(hw4_fin,T_K)
    K4 =  q4_fin/q4_ini*np.exp(-1.0*dH4/(kB*T_K))
    k4f = kBTh*q4_ts/q4_ini*np.exp(-1.0*Ea4f/(kB*T_K))
    k4r = kBTh*q4_ts/q4_fin*np.exp(-1.0*Ea4r/(kB*T_K))
    
    # 5
    q5_ini = pf_vib(hw5_ini,T_K)
    q5_ts =  pf_vib(hw5_ts,T_K)
    q5_fin = pf_vib(hw5_fin,T_K)
    K5 =  q5_fin/q5_ini*np.exp(-1*dH5/(kB*T_K))
    k5f = kBTh*q5_ts/q5_ini*np.exp(-1.0*Ea5f/(kB*T_K))
    k5r = kBTh*q5_ts/q5_fin*np.exp(-1.0*Ea5r/(kB*T_K))

    # 6
    q6_ini = pf_vib(hw6_ini,T_K)
    q6_fin = pf_trans(W_NH3,T_K)*pf_rot_nh3(T_K)
    K6 = q6_fin/q6_ini*np.exp(-1.0*dH6/(kB*T_K))

    # 7
    q7_ini = pf_trans(W_H2,T_K)*pf_rot_x2(Erot_H,T_K)*pf_vib(hw_H2,T_K)
    q7_fin = pf_vib(hw7_fin,T_K)
    K7 = q7_fin**2/q7_ini*np.exp(-1.0*dH7/(kB*T_K))
    
    # 8
    q8_ini = pf_vib(hw8_ini,T_K)
    q8_ts =  pf_vib(hw8_ts,T_K)
    q8_fin = pf_vib(hw8_fin,T_K)
    K8 =  q8_fin/q8_ini*np.exp(-1.0*dH8/(kB*T_K))
    k8f = kBTh*q8_ts/q8_ini*np.exp(-1.0*Ea8f/(kB*T_K))
    k8r = kBTh*q8_ts/q8_fin*np.exp(-1.0*Ea8r/(kB*T_K))

    return [[K1,K2,K3,K4,K5,K6,K7,K8],[k2f,k3f,k4f,k5f,k8f],[k2r,k3r,k4r,k5r,k8r]]

T = T_fin_dC
L_K = ""
cov_tot = []

while T >= 300.0:
    print(T)
    T_K = T + 273.15

    p_N2 = pp(T_K)[0] 
    p_H2 = pp(T_K)[1]
    p_NH3 = pp(T_K)[2]

    K1 = mc_ec(T_K)[0][0]
    K2 = mc_ec(T_K)[0][1]
    K3 = mc_ec(T_K)[0][2]
    K4 = mc_ec(T_K)[0][3]
    K5 = mc_ec(T_K)[0][4]
    K6 = mc_ec(T_K)[0][5]
    K7 = mc_ec(T_K)[0][6]
    K8 = mc_ec(T_K)[0][7]

    k2f = mc_ec(T_K)[1][0]
    k3f = mc_ec(T_K)[1][1]
    k4f = mc_ec(T_K)[1][2]
    k5f = mc_ec(T_K)[1][3]
    k8f = mc_ec(T_K)[1][4]

    k2r = mc_ec(T_K)[2][0]
    k3r = mc_ec(T_K)[2][1]
    k4r = mc_ec(T_K)[2][2]
    k5r = mc_ec(T_K)[2][3]
    k8r = mc_ec(T_K)[2][4]

    L_K += str(T) + " " + str(K1) + " " + str(K2) + " " + str(K3) + " " + str(K4) + " " + str(K5) + " " + str(K6) + " " + str(K7) + " " + str(K8) + "\n"
    
    # c_v = x[0], c_N2 = x[1], c_N = x[2], c_NH = x[3], c_NH2 = x[4], c_NH3 = x[5], c_Had = x[6], c_Hin = x[7], c_VH = x[8]
    """
    def rds_N2dis(x):
        return [x[0] + x[1] + x[2] + x[3] + x[4] + x[5] + x[6] -1,
                x[7] + x[8] -1,
                K1*p_N2*x[8]*x[0] - x[1],
                K3*x[2]*x[7] - x[3]*x[8],
                K4*x[3]*x[7] - x[4]*x[8],
                K5*x[4]*x[7] - x[5]*x[8],
                K6*x[5] - p_NH3*x[8]*x[0],
                K7*p_H2*x[0]**2 - x[6]**2,
                K8*x[6]*x[8] - x[7]*x[0]]

    cov = optimize.root(rds_N2dis, [0,0,0,0,0,0,0,0,0], method="lm", tol=1e-10, options={"xtol": 1e-12})
    """
    """
    x0,x1,x2,x3,x4,x5,x6,x7,x8 = sy.symbols("x0 x1 x2 x3 x4 x5 x6 x7 x8")
    eq0 = x0 + x1 + x2 + x3 + x4 + x5 + x6 -1
    eq1 = x7 + x8 -1
    eq2 = K1 * p_N2 * x8 * x0 - x1
    eq3 = K3 * x2 * x7 - x3 * x8
    eq4 = K4 * x3 * x7 - x4 * x8
    eq5 = K5 * x4 * x7 - x5 * x8
    eq6 = K6 * x5 - p_NH3 * x8 * x0
    eq7 = K7 * p_H2 * x0**2 -x6**2
    eq8 = K8 * x6 * x8 - x7 * x0
    """

    def cov_N2d():
        print("N2 dis.")
        x0,x1,x2,x3,x4,x5,x6,x7,x8 = sy.symbols("x0 x1 x2 x3 x4 x5 x6 x7 x8",real=True,nonzero=True,positive=True)
        eq0 = x0 + x1 + x2 + x3 + x4 + x5 + x6 -1.0
        eq1 = x7 + x8 -1.0
        eq2 = K1 * p_N2 * x8 * x0 - x1
        eq3 = K3 * x2 * x7 - x3 * x8
        eq4 = K4 * x3 * x7 - x4 * x8
        eq5 = K5 * x4 * x7 - x5 * x8
        eq6 = K6 * x5 - p_NH3 * x8 * x0
        eq7 = K7 * p_H2 * x0**2 - x6**2
        eq8 = K8 * x6 * x8 - x7 * x7 * x0
        
        anss = sy.solve([eq0,eq1,eq2,eq3,eq4,eq5,eq6,eq7,eq8],[x0,x1,x2,x3,x4,x5,x6,x7,x8])
        return list(anss[0])

    def cov_NHf():
        print("NH form.")
        x0,x1,x2,x3,x4,x5,x6,x7,x8 = sy.symbols("x0 x1 x2 x3 x4 x5 x6 x7 x8",real=True,nonzero=True,positive=True)
        eq0 = x0 + x1 + x2 + x3 + x4 + x5 + x6 -1.0
        eq1 = x7 + x8 -1.0
        eq2 = K1 * p_N2 * x8 * x0 - x1
        eq3 = K2 * x1 * x8 * x0  - x2**2 
        #eq3 = K3 * x2 * x7 - x3 * x8
        eq4 = K4 * x3 * x7 - x4 * x8
        eq5 = K5 * x4 * x7 - x5 * x8
        eq6 = K6 * x5 - p_NH3 * x8 * x0
        eq7 = K7 * p_H2 * x0**2 - x6**2
        eq8 = K8 * x6 * x8 - x7 * x7 * x0
            
        anss = sy.solve([eq0,eq1,eq2,eq3,eq4,eq5,eq6,eq7,eq8],[x0,x1,x2,x3,x4,x5,x6,x7,x8])
        return list(anss[0])

    def cov_NH2f():
        print("NH2 form.")
        x0,x1,x2,x3,x4,x5,x6,x7,x8 = sy.symbols("x0 x1 x2 x3 x4 x5 x6 x7 x8",real=True,nonzero=True,positive=True)
        eq0 = x0 + x1 + x2 + x3 + x4 + x5 + x6 -1
        eq1 = x7 + x8 -1
        eq2 = K1 * p_N2 * x8 * x0 - x1
        eq3 = K2 * x8 * x0 * x1 - x2**2
        eq4 = K3 * x2 * x7 - x3 * x8
        #eq4 = K4 * x3 * x7 - x4 * x8
        eq5 = K5 * x4 * x7 - x5 * x8
        eq6 = K6 * x5 - p_NH3 * x8 * x0
        eq7 = K7 * p_H2 * x0**2 -x6**2
        eq8 = K8 * x6 * x8 - x7 * x7 * x0

        anss = sy.solve([eq0,eq1,eq2,eq3,eq4,eq5,eq6,eq7,eq8],[x0,x1,x2,x3,x4,x5,x6,x7,x8])
        return list(anss[0])

    def cov_NH3f():
        print("NH3 form.")
        x0,x1,x2,x3,x4,x5,x6,x7,x8 = sy.symbols("x0 x1 x2 x3 x4 x5 x6 x7 x8",real=True,nonzero=True,positive=True)
        eq0 = x0 + x1 + x2 + x3 + x4 + x5 + x6 -1
        eq1 = x7 + x8 -1
        eq2 = K1 * p_N2 * x8 * x0 - x1
        eq3 = K2 * x8 * x0 * x1 - x2**2
        eq4 = K3 * x2 * x7 - x3 * x8
        eq5 = K4 * x3 * x7 - x4 * x8
        #eq5 = K5 * x4 * x7 - x5 * x8
        eq6 = K6 * x5 - p_NH3 * x8 * x0
        eq7 = K7 * p_H2 * x0**2 -x6**2
        eq8 = K8 * x6 * x8 - x7 * x7 * x0
        
        anss = sy.solve([eq0,eq1,eq2,eq3,eq4,eq5,eq6,eq7,eq8],[x0,x1,x2,x3,x4,x5,x6,x7,x8])
        return list(anss[0])

    def cov_Hex_x7x8():
        x7,x8 = sy.symbols("x7 x8",real=True,nonzero=True,positive=True)
        eq0 = x7 + x8 -1
        eq1 = (x7/x8)**3 - p_NH3/(np.sqrt(K1 * K2 * p_H2) * K3 * K4 * K5 * K6)
        anss = sy.solve([eq0,eq1],[x7,x8])
        return list(anss[0])
    
    x7 = cov_Hex_x7x8()[0]
    x8 = cov_Hex_x7x8()[1]

    def cov_Hex():
        print("H exch")
        x0,x1,x2,x3,x4,x5,x6= sy.symbols("x0 x1 x2 x3 x4 x5 x6",real=True,nonzero=True,positive=True)
        eq0 = x0 + x1 + x2 + x3 + x4 + x5 + x6 -1
        #eq1 = x7 + x8 -1
        #eq1 = K1 * p_N2 * x8 * x0 - x1
        eq1 = K2 * x8 * x0 * x1 - x2**2
        eq2 = K3 * x2 * x7 - x3 * x8
        eq3 = K4 * x3 * x7 - x4 * x8
        eq4 = K5 * x4 * x7 - x5 * x8
        eq5 = K6 * x5 - p_NH3 * x8 * x0
        eq6 = K7 * p_H2 * x0**2 -x6**2
        #eq8 = (x7/x8)**3 - p_NH3/(np.sqrt(K1 * K2 * p_H2) * K3 * K4 * K5 * K6)
        #eq8 = K8 * x6 * x8 - x7 * x7 * x0
    
        anss = sy.solve([eq0,eq1,eq2,eq3,eq4,eq5,eq6],[x0,x1,x2,x3,x4,x5,x6])
        return list(anss[0]) + [x7,x8]

    cov = [T] + cov_N2d() + [T] + cov_NHf() + [T] + cov_NH2f() + [T] + cov_NH3f() + [T] + cov_Hex()
    cov_tot.append(cov)
    
    #rate_n2dis = k2f*cov.x[1]*cov.x[8]*cov.x[0] - k2r*cov.x[2]**2
    #L_R_N2dis += str(T) + " " + str(1/(kB*T_K)) + " " + str(cov.x[0]) + " " + str(cov.x[1]) + " " + str(cov.x[2]) + " " + str(cov.x[3]) + " " + str(cov.x[4]) + " " + str(cov.x[5]) + " " +  str(cov.x[6]) + " " + str(cov.x[7]) + " " + str(cov.x[8]) + "\n" 

    T = T -10

L = ""
for cov in cov_tot:
    for val in cov:
        L += str(val) + " "
    else:
        L += "\n"
        
path_cov_tot = path + "/cov_tot.dat"
f = open(path_cov_tot, "w")
f.write(L)
f.close()
        
print("Done!")

"""
path_K = path + "/K.dat"
f = open(path_K,"w")
f.write(L_K)
f.close()

path_rdsN2dis = path + "/rds_N2dis.dat"
f = open(path_rdsN2dis,"w")
f.write(L_R_N2dis)
f.close()
"""
           
"""
T = T_fin_dC
L_pf = ""
L_pp = ""
while T >= 200:
    T_K = T + 273.15
    L_pf += str(T) + " " + str(pf_trans(W_NH3,T_K)) + " " + str(pf_rot_nh3(T_K)) + "\n " 
    #L_pp += str(T) + " " + str(pp(T_K)[0]) + " " + str(pp(T_K)[1]) + " " + str(pp(T_K)[2]) + "\n" 
    T = T -5

path_pf = path + "/pf_NH3.dat"
f = open(path_pf,"w")
f.write(L_pf)
f.close()
"""

#path_pp = path + "/pp.dat"
#f = open(path_pp,"w")
#f.write(L_pp)
#f.close()
