#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os
from stat import *
import os.path
import shutil
import sys
import glob
import subprocess as sub
import fractions as fra
from functools import reduce


############################ INPUT ZONE ###########################
path_vasp = ""
path_qsub = "/apps/t3/sles12sp2/uge/latest/bin/lx-amd64/qsub"
adsites = ["ontop","bridge","hollow"]
############################ INPUT ZONE ###########################

def gcd_list(numbers):
    return reduce(fra.gcd, numbers)

def conv_line(line):
    line = line.replace('\n',' ')
    line = line.replace('\r',' ')
    line = line.replace('\t',' ')
    line = line.replace('^M',' ')
    line = line.split(" ")
    while line.count("") > 0:
        line.remove("")
    return line

def mkdv(jobname,nodetype,numnode,process,jobtime):
    Lsh =  "#!/bin/sh\n"    
    Lsh += "#$-cwd\n"
    Lsh += "#$ -l " + nodetype + "=" + numnode + "\n"
    Lsh += "#$ -l h_rt=" + jobtime + "\n"
    Lsh += "#$ -N " + jobname + "\n"
    Lsh += ". /etc/profile.d/modules.sh\n"
    Lsh += "module load cuda intel intel-mpi fftw\n\n"
    Lsh += "date >> date.txt\n"
    Lsh += "mpirun -n " + process + " " + path_vasp +  " > std.out\n"
    Lsh += "date >> date.txt\n"
    return Lsh

def chgINCARforIBE(path_INCAR):
    f_in = open(path_INCAR)
    incarlines = f_in.readlines()
    f_in.close()
    
    num = 0
    while num < len(incarlines):
        if incarlines[num].find("ISYM = ") >= 0:
            incarlines[num] = "ISYM = -1\n"
        elif incarlines[num].find("SYMPREC = ") >= 0:
            incarlines[num] = "SYMPREC = 1e-20\n"
        num += 1

    os.remove(path_INCAR)
    f_in = open(path_INCAR, "w")
    for line in incarlines:
        f_in.write(line)
    f_in.close()

def chgKPOINTSforIBE(path_KPOINTS, path_dir_kp):
    f_k = open(path_KPOINTS)
    kplines = f_k.readlines()
    f_k.close()
    kplist = [conv_line(x) for x in kplines]

    os.chdir(path_dir_kp)
    mkkp = "mkkp.py " + str(kplist[3][0]) + " " + str(kplist[3][1]) + " " + str(kplist[3][2]) + " G"
    sub.call(mkkp,shell=True)

def rewrite_vi(path_vi,jobtime):
    fi = open(path_vi)
    ilines = fi.readlines()
    fi.close()

    num_i = 0
    while num_i < len(ilines):
        if ilines[num_i].find("kp_method:") >= 0:
            ilines[num_i] = "kp_method: G \n"
        elif ilines[num_i].find("job_time:") >= 0:
            ilines[num_i] = "job_time: " + jobtime + "\n"
        num_i += 1
        
    fi = open(path_vi, "w")
    for line in ilines:
        fi.write(line)
    fi.close()

path_qsub = "/apps/t3/sles12sp2/uge/latest/bin/lx-amd64/qsub"
adsites = ["ontop","bridge","hollow"]

path = os.getcwd()
argv = sys.argv

if len(argv) == 1:
    print("\n")
    print("##########################################################################################################")
    print("#    You should input some comannds. Please input -h or -help if you want to know the commands. Bye!     #")
    print("##########################################################################################################")
    print("\n")
    sys.exit()

### Print Help ###
command = str(argv[1])
if command in ["-h", "-help"]:
    print("\n")
    print("-tq-s : test qsub for surface calculation.")
    print("-tq-ad: test qsub for adsorption calculation.")
    print("-tq-list: test qsub for list wrriten in.")
    print("-rtq: re-test qsub.")
    print("-aq-ad: actual qsub for adsorption calculation.")
    print("-raq-s: actual qsub for surface calculation.")
    print("-raq-ad: re-actual calculation for adsorption calculation.")
    print("\n")
    sys.exit()
### Fin ###

### Prepare s-list and POSCAR from bulk calc. ###
if command in ["-pr"]:
    ### Read s-list ###
    path_slist = path + "/s-list"
    flag_s = os.path.exists(path_slist)
    if flag_s == False:
        print("\n")
        print("#########################################################")
        print("#   s-list does not exist. You should prepare s-list.   #")
        print("#########################################################")
        print("\n")
        sys.exit()

    f = open(path_slist)
    slines = f.readlines()
    f.close()
    
    slist = []
    for line in slines:
        line = line.replace('\n','')
        line = line.replace("JobName:","")
        line = line.replace("cutoff:","")
        line = line.replace("k-mesh_in_bulk:","")
        line = line.replace("pseudpot:","")
        line = line.split(" ")
        while line.count("") > 0:
            line.remove("")
        slist.append(line)
    ### Fin ###

    path_cut700 = glob.glob("../bulk/cut7*")
    path_cut700_out = path_cut700[0] + "/OUTCAR"
    path_cut700_cont = path_cut700[0] + "/CONTCAR"
    path_pos = path + "/POSCAR"    
    shutil.copy(path_cut700_cont, path_pos)    

    # Check presence of CONTCAR
    flag_cont = os.path.exists(path_cut700_out)
    if flag_cont == False:
        print("\n")
        print("#######################################")
        print("#     CONTCAR doesn't exist! Bye!     #")
        print("#######################################")
        print("\n")
        sys.exit()

    # Check presence of OUTCAR
    flag_out = os.path.exists(path_cut700_out)
    if flag_out == False:
        print("\n")
        print("#######################################")
        print("#     OUTCAR doesn't exist! Bye!      #")
        print("#######################################")
        print("\n")
        sys.exit()

    # Read CONTCAR
    contcar = open(path_cut700_cont)
    contlines = contcar.readlines()
    contcar.close()
    
    atomlabel = conv_line(contlines[5])
    atomnum = conv_line(contlines[6])

    print(list(map(int,atomnum)))
    gcd = gcd_list(list(map(int,atomnum)))
    print(gcd)
    
    atomnum_gcd = []
    for num in map(int,atomnum):
        print(num/gcd)
        atomnum_gcd.append(num/gcd)

    jobname = ""
    num_l = 0
    while num_l < len(atomlabel):
        if atomnum_gcd[num_l] == 1:
            jobname += str(atomlabel[num_l])
        else:
            jobname += str(atomlabel[num_l]) + str(atomnum_gcd[num_l])
        num_l += 1

    L_atoms_forpot = ""
    for label in atomlabel:
        L_atoms_forpot += str(label) + " "

    sumatomnum = 0
    for num_a in atomnum:
        sumatomnum += int(num_a)

    # Read OUTCAR
    outcar = open(path_cut700_out)
    outlines = outcar.readlines()
    outcar.close()
    
    num1 = 0
    list_totalenergy = []
    while num1 < len(outlines):
        if outlines[num1].find("FREE ENERGIE OF THE ION-ELECTRON SYSTEM") >= 0:
            num1 += 4
            toten = outlines[num1]
            toten = toten.replace('\n','')
            toten = toten.replace('\r','')
            toten = toten.replace('\t',' ')
            toten = toten.split(" ")
            list_totalenergy.append(toten[-1])
        else:
            num1 += 1
    Etot = str(list_totalenergy[-1])

    num_s = 0
    while num_s < len(slines):
        if slines[num_s].find("JobName:") >= 0:
            value= "JobName:" + jobname + "\n"
            slines[num_s] = value
        elif slines[num_s].find("Etot_bulk:") >= 0:
            value= "Etot_bulk:" + Etot + "\n"
            slines[num_s] = value
        elif slines[num_s].find("Natom_bulk:") >= 0:
            value= "Natom_bulk:" + str(sumatomnum) + "\n"
            slines[num_s] = value
        elif slines[num_s].find("pseudpot:") >= 0:
            value= "pseudpot:" + L_atoms_forpot + "\n"
            slines[num_s] = value
        num_s += 1
            
    f_s = open(path_slist,'w')
    for line in slines:
        f_s.write(line)
    f_s.close()
    
    print("\n")
    print("########################################")
    print("#        s-list was changed!           #")
    print("########################################")
    print("\n")

    sys.exit()
### Fin ###

qsubtest = path_qsub + " testjob.sh"
qsubact = path_qsub + " job.sh"
### qsub fisrt
if command in ["-tq-s"]:
    dirs_hkl = [x for x in os.listdir(path) if os.path.isdir(path+"/"+x) and "Relax" not in x]

    print(dirs_hkl)
    for dir_hkl in dirs_hkl:
        path_dir = path + "/" + dir_hkl
        dirs_in_hkl = [x for x in os.listdir(path_dir) if os.path.isdir(path_dir+"/"+x) and "Relax" in x]
        dirs_in_hkl.sort()

        print(dirs_in_hkl)
        for relaxdir in dirs_in_hkl:
            Lp = relaxdir + " in " + dir_hkl
            print(Lp)
            path_dir_relax = path_dir + "/" + relaxdir

            os.chdir(path_dir_relax)
            sub.call(qsubtest,shell=True)
    sys.exit()

### qsub fisrt for adsorption
if command in ["-tq-ad"]:
    dirs_ad = [x for x in os.listdir(path) if os.path.isdir(path+"/"+x) and "ontop" in x or "bridge" in x or "hollow" in x]
    print(dirs_ad)
    for dir_ad in dirs_ad:
        path_dir = path + "/" + dir_ad
        os.chdir(path_dir)
        sub.call(qsubtest,shell=True)
    sys.exit()

### qsub fisrt for adsorption
if command in ["-aq-ad"]:
    dirs_ad = [x for x in os.listdir(path) if os.path.isdir(path+"/"+x) and "ontop" in x or "bridge" in x or "hollow" in x]
    print(dirs_ad)
    for dir_ad in dirs_ad:
        path_dir = path + "/" + dir_ad
        os.chdir(path_dir)
        sub.call(qsubact,shell=True)
    sys.exit()

### Recalculate testjob! ###
if command in ["-rtq"]:
    print("\n")
    print("##########################################################")
    print("#               Recalculate  testjob!                    #")
    print("##########################################################")
    print("\n")

    path_SE = path + "/Sum_TotalandSurfaceEnergy.dat"
    if os.path.exists(path_SE) == False:
        print("\n")
        print("############################################################")
        print("#    Sum_TotalandSurfaceEnergy.dat doesn't exist! Bye!     #")
        print("############################################################")
        print("\n")
        sys.exit()
        
    f_SE = open(path_SE)
    SElines = f_SE.readlines()
    f_SE.close()
    list_SE = [conv_line(line) for line in SElines]
    del list_SE[0]

    for hkl in list_SE:
        if "R" in hkl[3]:
            if "R" in hkl[7]:
                pass
            elif "N" in hkl[7]:
                path_dir_prec = path + "/" + hkl[0] + "/" + hkl[1] + "/" + hkl[6]
                print("\n" + hkl[0] + " " + hkl[1] + " " + hkl[6] + "\n")

                # read vauto-input
                path_input = path + "/" + hkl[0] + "/" + hkl[1] + "/vauto-input"
                fv = open(path_input)
                ilines = fv.readlines()
                fv.close()
                ilist = [conv_line(line) for line in ilines if len(conv_line(line)) > 0]
                for tag in ilist:
                    if "job_name:" in tag:
                        job_name = str(tag[1])

                # make backup
                os.chdir(path_dir_prec)
                if "N0" not in hkl[7] and "Nn" not in hkl[7]:
                    sub.call("mkbackup.py",shell=True)
                    
                # make testjob.sh
                path_job = path_dir_prec + "/testjob.sh"
                if os.path.exists(path_job) == False:
                    Ljob = mkdv(job_name+"_test","s_gpu","2","2","0:10:00")
                    fj = open(path_job,"w")
                    fj.write(Ljob)
                    fj.close()
                    os.chmod(path_job, S_IRUSR | S_IWUSR | S_IXUSR)

                # qsub
                INCAR_test = path_dir_prec + "/INCAR_test"
                INCAR = path_dir_prec + "/INCAR"
                shutil.copy(INCAR_test,INCAR)

                sub.call(qsubtest,shell=True)
                os.chdir(path)

            elif "IBE" in hkl[7]:
                path_dir_prec = path + "/" + hkl[0] + "/" + hkl[1] + "/" + hkl[6]
                print("\n" + hkl[0] + " " + hkl[1] + " " + hkl[6] + "\n")

                # change INCARs and KPOINTS
                INCAR_test = path_dir_prec + "/INCAR_test"
                INCAR_act = path_dir_prec + "/INCAR_act"
                KPOINTS = path_dir_prec + "/KPOINTS"
                chgINCARforIBE(INCAR_test)
                chgINCARforIBE(INCAR_act)
                chgKPOINTSforIBE(KPOINTS, path_dir_prec)

                # read vauto-input
                path_input = path + "/" + hkl[0] + "/" + hkl[1] + "/vauto-input"
                fv = open(path_input)
                ilines = fv.readlines()
                fv.close()
                ilist = [conv_line(line) for line in ilines if len(conv_line(line)) > 0]
                for tag in ilist:
                    if "job_name:" in tag:
                        job_name = str(tag[1])

                # make job.sh
                Ljob = mkdv(job_name+"_test","s_gpu","2","2","0:10:00")
                path_job = path_dir_prec + "/testjob.sh"
                fj = open(path_job,"w")
                fj.write(Ljob)
                fj.close()
                os.chmod(path_job, S_IRUSR | S_IWUSR | S_IXUSR)
                
                # qsub
                INCAR_test = path_dir_prec + "/INCAR_test"
                INCAR = path_dir_prec + "/INCAR"
                shutil.copy(INCAR_test,INCAR)

                sub.call(qsubtest,shell=True)
                os.chdir(path)

        elif "N" in hkl[3]:
            path_dir_relax = path + "/" + hkl[0] + "/" + hkl[1]
            path_dir_prec = path + "/" + hkl[0] + "/" + hkl[1] + "/" + hkl[2]
            print("\n" + hkl[0] + " " + hkl[1] + " " + hkl[2] + "\n")

            # make backup
            os.chdir(path_dir_prec)
            if "N0" not in hkl[3] and "Nn" not in hkl[3]:
                sub.call("mkbackup.py",shell=True)
            
            # qsub
            os.chdir(path_dir_relax)
            sub.call(qsubtest,shell=True)
            os.chdir(path)

        elif "IBE" in hkl[3]:
            path_dir_relax = path + "/" + hkl[0] + "/" + hkl[1]
            path_dir_precM = path + "/" + hkl[0] + "/" + hkl[1] + "/" + hkl[2]
            path_dir_precN = path + "/" + hkl[0] + "/" + hkl[1] + "/" + hkl[6]
            print("\n" + hkl[0] + " " + hkl[1] + " " + hkl[2] + "\n")

            # remove directories
            calcdirs_in_relax = [x for x in os.listdir(path_dir_relax) if "Prec" in x]
            for dir_prec in calcdirs_in_relax:
                path_dir_prec = path_dir_relax + "/" + dir_prec
                shutil.rmtree(path_dir_prec)
            
            # change INCAR
            path_calc = path_dir_relax + "/calc"
            INCAR_calc = path_calc + "/INCAR"
            chgINCARforIBE(INCAR_calc)

            # rewrite vauto-input
            path_input = path_dir_relax + "/vauto-input"
            rewrite_vi(path_input,"0:10:00")
 
            # remake PREC-directories
            os.chdir(path_dir_relax)
            sub.call("mkdfmr.py",shell=True)

            # qsub
            sub.call(qsubtest,shell=True)
            os.chdir(path)

    sys.exit()


## Recalculate 
if command in ["-tq-list"]:
    print("\n")
    print("###################################################################")
    print("#     Recalculate testjob from Sum_TotalandSurfaceEnergy.dat      #")
    print("###################################################################")
    print("\n")

    path_SE = path + "/Sum_TotalandSurfaceEnergy.dat"
    if os.path.exists(path_SE) == False:
        print("\n")
        print("############################################################")
        print("#    Sum_TotalandSurfaceEnergy.dat doesn't exist! Bye!     #")
        print("############################################################")
        print("\n")
        sys.exit()

    f_SE = open(path_SE)
    SElines = f_SE.readlines()
    f_SE.close()
    list_SE = [conv_line(line) for line in SElines]
    del list_SE[0]

    for hkl in list_SE:
        Lp = hkl[1] + " in " + hkl[0]
        print(Lp)

        os.chdir(path_dir_relax)
        sub.call(qsubtest,shell=True)

    sys.exit()

## Recalculate job from Sum_TotalandSurfaceEnergy.dat ##
if command in ["-raq-s"]:
    print("\n")
    print("#################################################################")
    print("#     Recalculate job from Sum_TotalandSurfaceEnergy.dat!       #")
    print("#################################################################")
    print("\n")

    if os.path.exists(path + "/Sum_TotalandSurfaceEnergy.dat") == True:
        path_SE = path + "/Sum_TotalandSurfaceEnergy.dat"
        print("\n")
        print("############################################################")
        print("#          Sum_TotalandSurfaceEnergy.dat exists!           #")
        print("############################################################")
        print("\n")

    else:
        print("\n")
        print("############################################################")
        print("#    Sum_TotalandSurfaceEnergy.dat doesn't exist! Bye!     #")
        print("############################################################")
        print("\n")
        sys.exit()
        
    f_SE = open(path_SE)
    SElines = f_SE.readlines()
    f_SE.close()
    list_SE = [conv_line(line) for line in SElines]
    del list_SE[0]

    job_time = input("Please input the job time you want, i.e., 1:00:00: ")
    for hkl in list_SE:
        if "R" in hkl[3]:
            if "R" in hkl[7]:
                pass
            elif "N" in hkl[7]:
                path_dir_prec = path + "/" + hkl[0] + "/" + hkl[1] + "/" + hkl[6]
                print("\n" + hkl[0] + " " + hkl[1] + " " + hkl[6] + "\n")

                # read vauto-input
                path_input = path + "/" + hkl[0] + "/" + hkl[1] + "/vauto-input"
                fv = open(path_input)
                ilines = fv.readlines()
                fv.close()

                ilist = [conv_line(line) for line in ilines if len(conv_line(line)) > 0]
                for tag in ilist:
                    if "job_name:" in tag:
                        job_name = str(tag[1])
                    elif "node_type:" in tag:
                        node_type = str(tag[1])
                    elif "num_node:" in tag:
                        num_node = str(tag[1])
                    elif "process:" in tag:
                        process = str(tag[1])
                    
                # make backup
                os.chdir(path_dir_prec)
                if "N0" not in hkl[7] and "Nn" not in hkl[7]:
                    sub.call("mkbackup.py",shell=True)

                # make job.sh
                Ljob = mkdv(job_name,node_type,num_node,process,job_time)
                path_job = path_dir_prec + "/job.sh"
                fj = open(path_job,"w")
                fj.write(Ljob)
                fj.close()
                os.chmod(path_job, S_IRUSR | S_IWUSR | S_IXUSR)

                # qsub
                INCAR_act = path_dir_prec + "/INCAR_act"
                INCAR = path_dir_prec + "/INCAR"
                shutil.copy(INCAR_act,INCAR)

                sub.call(qsubact,shell=True)
                os.chdir(path)

            elif "IBE" in hkl[7]:
                path_dir_prec = path + "/" + hkl[0] + "/" + hkl[1] + "/" + hkl[6]
                print("\n" + hkl[0] + " " + hkl[1] + " " + hkl[6] + "\n")

                # change INCARs and KPOINTS
                INCAR_test = path_dir_prec + "/INCAR_test"
                INCAR_act = path_dir_prec + "/INCAR_act"
                KPOINTS = path_dir_prec + "/KPOINTS"
                chgINCARforIBE(INCAR_test)
                chgINCARforIBE(INCAR_act)
                chgKPOINTSforIBE(KPOINTS, path_dir_prec)

                # read vauto-input
                path_input = path + "/" + hkl[0] + "/" + hkl[1] + "/vauto-input"
                fv = open(path_input)
                ilines = fv.readlines()
                fv.close()

                ilist = [conv_line(line) for line in ilines if len(conv_line(line)) > 0]
                for tag in ilist:
                    if "job_name:" in tag:
                        job_name = str(tag[1])
                    elif "node_type:" in tag:
                        node_type = str(tag[1])
                    elif "num_node:" in tag:
                        num_node = str(tag[1])
                    elif "process:" in tag:
                        process = str(tag[1])

                # make job.sh
                Ljob = mkdv(job_name,node_type,num_node,process,job_time)
                path_job = path_dir_prec + "/job.sh"
                fj = open(path_job,"w")
                fj.write(Ljob)
                fj.close()
                os.chmod(path_job, S_IRUSR | S_IWUSR | S_IXUSR)


                # qsub
                INCAR_act = path_dir_prec + "/INCAR_act"
                INCAR = path_dir_prec + "/INCAR"
                shutil.copy(INCAR_act,INCAR)
                sub.call(qsubact,shell=True)
                os.chdir(path)

        elif "N" in hkl[3]:
            path_dir_relax = path + "/" + hkl[0] + "/" + hkl[1]
            path_dir_prec = path + "/" + hkl[0] + "/" + hkl[1] + "/" + hkl[2]
            print("\n" + hkl[0] + " " + hkl[1] + " " + hkl[2] + "\n")

            # make backup
            os.chdir(path_dir_prec)
            if "N0" not in hkl[3] and "Nn" not in hkl[3]:
                sub.call("mkbackup.py",shell=True)
            
            # rewrite job.sh
            path_job = path_dir_relax + "/job.sh"
            fj = open(path_job)
            joblines = fj.readlines()
            fj.close()

            Lj = ""
            for jline in joblines:
                if jline.find("#$ -l h_rt=") >= 0:
                    jline = "#$ -l h_rt=" + job_time + "\n"
                Lj += jline

            fj = open(path_job,"w")
            fj.write(Lj)
            fj.close()

            # qsub
            os.chdir(path_dir_relax)
            sub.call(qsubact,shell=True)
            os.chdir(path)

        elif "IBE" in hkl[3]:
            path_dir_relax = path + "/" + hkl[0] + "/" + hkl[1]
            print("\n" + hkl[0] + " " + hkl[1] + " " + hkl[2] + "\n")

            # remove directories
            calcdirs_in_relax = [x for x in os.listdir(path_dir_relax) if "Prec" in x]
            for dir_prec in calcdirs_in_relax:
                path_dir_prec = path_dir_relax + "/" + dir_prec
                shutil.rmtree(path_dir_prec)

            # rewrite INCAR
            path_calc = path_dir_relax + "/calc"
            INCAR_calc = path_calc + "/INCAR"
            chgINCARforIBE(INCAR_calc)

            # rewrite vatuo-input
            path_input = path_dir_relax + "/vauto-input"
            rewrite_vi(path_input,job_time)

            # rename PREC-directories
            os.chdir(path_dir_relax)
            sub.call("mkdfmr.py",shell=True)

            # qsub
            sub.call(qsubact,shell=True)
            os.chdir(path)

    sys.exit()


# Recalculate job from Sum_TotalandSurfaceEnergy.dat ##
if command in ["-raq-ad"]:
    print("\n")
    print("#################################################################")
    print("#     Recalculate job from Sum_TotalandSurfaceEnergy.dat!       #")
    print("#################################################################")
    print("\n")


    if os.path.exists(path + "/Sum_Eads.dat") == True:
        path_SE = path + "/Sum_Eads.dat"
        print("\n")
        print("############################################################")
        print("#                  Sum_Eads.dat exists!                    #")
        print("############################################################")
        print("\n")

    else:
        print("\n")
        print("############################################################")
        print("#             Sum_Eads.dat doesn't exist! Bye!             #")
        print("############################################################")
        print("\n")
        sys.exit()

    f_SE = open(path_SE)
    SElines = f_SE.readlines()
    f_SE.close()
    list_SE = [conv_line(line) for line in SElines]
    del list_SE[0]

    job_time = input("Please input the job time you want, i.e., 1:00:00: ")
    for hkl in list_SE:
        if "R" in hkl[2]:
            if "R" in hkl[5]:
                pass
            elif "N" in hkl[5]:
                path_dir_prec = path + "/" + hkl[0] + "/" + hkl[5] 
                print("\n" + hkl[0] + " " + hkl[5] + "\n")

                # read vauto-input
                path_input = path + "/" + hkl[0] + "/vauto-input"
                fv = open(path_input)
                ilines = fv.readlines()
                fv.close()

                ilist = [conv_line(line) for line in ilines if len(conv_line(line)) > 0]
                for tag in ilist:
                    if "job_name:" in tag:
                        job_name = str(tag[1])
                    elif "node_type:" in tag:
                        node_type = str(tag[1])
                    elif "num_node:" in tag:
                        num_node = str(tag[1])
                    elif "process:" in tag:
                        process = str(tag[1])

                # make backup
                os.chdir(path_dir_prec)
                if "N0" not in hkl[7] and "Nn" not in hkl[7]:
                    sub.call("mkbackup.py",shell=True)

                # make job.sh
                Ljob = mkdv(job_name,node_type,num_node,process,job_time)
                path_job = path_dir_prec + "/job.sh"
                fj = open(path_job,"w")
                fj.write(Ljob)
                fj.close()
                os.chmod(path_job, S_IRUSR | S_IWUSR | S_IXUSR)

                # qsub
                INCAR_act = path_dir_prec + "/INCAR_act"
                INCAR = path_dir_prec + "/INCAR"
                shutil.copy(INCAR_act,INCAR)

                sub.call(qsubact,shell=True)
                os.chdir(path)

            elif "IBE" in hkl[7]:
                path_dir_prec = path + "/" + hkl[0] + "/" + hkl[5]
                print("\n" + hkl[0] + " " + hkl[5] + "\n")

                # change INCARs and KPOINTS
                INCAR_test = path_dir_prec + "/INCAR_test"
                INCAR_act = path_dir_prec + "/INCAR_act"
                KPOINTS = path_dir_prec + "/KPOINTS"
                chgINCARforIBE(INCAR_test)
                chgINCARforIBE(INCAR_act)
                chgKPOINTSforIBE(KPOINTS, path_dir_prec)

                # read vauto-input
                path_input = path + "/" + hkl[0] + "/vauto-input"
                fv = open(path_input)
                ilines = fv.readlines()
                fv.close()

                ilist = [conv_line(line) for line in ilines if len(conv_line(line)) > 0]
                for tag in ilist:
                    if "job_name:" in tag:
                        job_name = str(tag[1])
                    elif "node_type:" in tag:
                        node_type = str(tag[1])
                    elif "num_node:" in tag:
                        num_node = str(tag[1])
                    elif "process:" in tag:
                        process = str(tag[1])

                # make job.sh
                Ljob = mkdv(job_name,node_type,num_node,process,job_time)
                path_job = path_dir_prec + "/job.sh"
                fj = open(path_job,"w")
                fj.write(Ljob)
                fj.close()
                os.chmod(path_job, S_IRUSR | S_IWUSR | S_IXUSR)

                # qsub
                INCAR_act = path_dir_prec + "/INCAR_act"
                INCAR = path_dir_prec + "/INCAR"
                shutil.copy(INCAR_act,INCAR)
                sub.call(qsubact,shell=True)
                os.chdir(path)

        elif "N" in hkl[2]:
            path_dir_relax = path + "/" + hkl[0] 
            path_dir_prec = path + "/" + hkl[0] + "/" + hkl[1] 
            print("\n" + hkl[0] + " " + hkl[1] + "\n")

            # make backup
            os.chdir(path_dir_prec)
            if "N0" not in hkl[2] and "Nn" not in hkl[2]:
                sub.call("mkbackup.py",shell=True)

            # rewrite job.sh
            path_job = path_dir_relax + "/job.sh"
            fj = open(path_job)
            joblines = fj.readlines()
            fj.close()

            Lj = ""
            for jline in joblines:
                if jline.find("#$ -l h_rt=") >= 0:
                    jline = "#$ -l h_rt=" + job_time + "\n"
                Lj += jline

            fj = open(path_job,"w")
            fj.write(Lj)
            fj.close()

            # qsub
            os.chdir(path_dir_relax)
            sub.call(qsubact,shell=True)
            os.chdir(path)

        elif "IBE" in hkl[2]:
            path_dir_relax = path + "/" + hkl[0] 
            print("\n" + hkl[0] + " " + hkl[1] + "\n")

            # remove directories
            calcdirs_in_relax = [x for x in os.listdir(path_dir_relax) if "Prec" in x]
            for dir_prec in calcdirs_in_relax:
                path_dir_prec = path_dir_relax + "/" + dir_prec
                shutil.rmtree(path_dir_prec)

            # rewrite INCAR
            path_calc = path_dir_relax + "/calc"
            INCAR_calc = path_calc + "/INCAR"
            chgINCARforIBE(INCAR_calc)

            # rewrite vatuo-input
            path_input = path_dir_relax + "/vauto-input"
            rewrite_vi(path_input,job_time)

            # rename PREC-directories
            os.chdir(path_dir_relax)
            sub.call("mkdfmr.py",shell=True)

            # qsub
            sub.call(qsubact,shell=True)
            os.chdir(path)

    sys.exit()

