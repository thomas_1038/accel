#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#2017/02/07 update
#2017/10/27 update
#make .sh file

import os, sys, shutil
from stat import *

def make_dovasp(num_node, job_name, vasptype="vasp541neb_std",path_vasp="/home/nakao/vasp/bin/", num_core=16):
	Lsh = "#!/bin/sh\n"
	Lsh += "#PBS -l nodes=" + str(num_node) + ":ppn=" + str(num_core) + "\n"
	Lsh += "#PBS -N " + job_name + "\n"
	Lsh += "cd $PBS_O_WORKDIR\n"
	Lsh += "NPROCS=`wc -l <$PBS_NODEFILE`\n"
	Lsh += "export MKL_NUM_THREADS=1\n\n"
	Lsh += "path_vasp=\"" + str(path_vasp) + "\"\n"
	Lsh += "vasp_type=\"" + str(vasptype) + "\"\n"
	Lsh += "process=" + str(int(num_core)) +  "\n\n"
	Lsh += "echo \"NEW JOB\" >> date.txt\n"
	Lsh += "date >> date.txt\n"
	Lsh += "mpirun -n $process $path_vasp$vasp_type > std.out\n"
	Lsh += "date >> date.txt\n\n"
	return Lsh

if __name__ == "__main__":
	path = os.getcwd()
	argv = sys.argv

	if len(argv) > 1:
		if str(argv[1]) == "-h": print("\n-h: help\n-d: make job.sh default version\n-j: input your job name\n"); sys.exit()
		elif str(argv[1]) == "-d": job_name = "job"; N = "1"; Q_vasptype = "d"
		elif str(argv[1]) == "-j": job_name = str(argv[2]); N = 1; Q_vasptype = "d"

	elif len(argv) == 1:
		job_name  = input("Please input job name: ")
		N = input("Please input the number of node or group1: ")
		Q_vasptype = input("Please input the type of vasp, [vasp541_std,s / vasp541_ncl,nc / vasp541_gam,g / vasp541wan_std,w / vasp541neb_std,ne / default,d]: ")

	# node_name
	if N in ["a","accel"]: num_node = "accel"
	else: num_node = str(N)

	# vasp_type
	if Q_vasptype in ["d"]: vasptype = "vasp541_std"
	elif Q_vasptype in ["s","std","vasp541_std"]: vasptype = "vasp541_std"
	elif Q_vasptype in ["nc","ncl","vasp541_ncl"]: vasptype = "vasp541_ncl"
	elif Q_vasptype in ["g","gam","vasp541_gam"]: vasptype = "vasp541_gam"
	elif Q_vasptype in ["w","wan","vasp541wan_std"]: vasptype = "vasp541wan_std"
	elif Q_vasptype in ["ne","neb","vasp541neb_std"]: vasptype = "vasp541neb_std"

	Lsh = make_dovasp(num_node, job_name)

	with open(path + "/job.sh", "w") as fj: fj.write(Lsh)
	os.chmod(path + "/job.sh", S_IXUSR | S_IRUSR | S_IWUSR )
	print("\njob.sh has been made!\n")

#END: Program
