#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os, sys, shutil
import subprocess as sub
from get import getmaxforce as gm

flag_ra = "reached required accuracy - stopping structural energy minimisation"
flag_wa = "writing wavefunctions"
path_home = "/home/nakao"
nodename = "accel"
Lhelp ="\n-h: help mode\n-ea: show Ea\n"

def qsdir():
    def adjust_len(list_qd):
        list_new = []; list_new_ap = list_new.append
        max_len = max([len(x) for x in list_qd])
    
        L = ""
        for i in range(0,max_len): L += "-"
        for l in list_qd:
            str_num = max_len - len(l)
            n = 1
            while n < str_num +1: l += " "; n += 1
            list_new_ap(l)
    
        list_new.insert(1,L)
        return list_new
    
    def conv_line(line):
        line = line.replace('\n',' ').replace('\r',' ').replace('\t',' ').replace('^M',' ')
        line = line.split(" ")
        while line.count("") > 0: line.remove("")
        return line

    def last_force(path_re):
        gm.get_maxforce(path_re)
        with open(path_re + "/MAXFORCE") as fm: mflines = fm.readlines()
        last_f = [float(conv_line(x)[2]) for x in mflines][-1]; last_f = "%.3f" % last_f
        f_label = [conv_line(x)[3] for x in mflines][-1]
        return last_f, f_label
    
    path = os.getcwd()
    argv = sys.argv
    if len(argv) > 1 and argv[1] == "-h": return Lhelp

    path_qstat = path + "/qstat.dat"; qstat = "qstat -f > qstat.dat"
    sub.call(qstat,shell=True)
    path_qstat = path + "/qstat.dat"; flag_qstat = os.path.exists(path_qstat)
    if flag_qstat == False: print("qstat.dat does not exsits!"); sys.exit()
    
    #read qstat.dat                                              
    with open(path_qstat) as f: qstatlines = f.readlines()
    if len(qstatlines) == 0: print("There are no jobs! BYE!"); sys.exit()
    os.remove(path_qstat)
    
    JobID = ["job-ID"]; JobID_append = JobID.append
    JobName = ["jobname"]; JobName_append = JobName.append
    JobOwner = ["user"]; JobOwner_append = JobOwner.append
    walltime = ["walltime"]; walltime_append = walltime.append
    job_state = ["S"]; job_state_append = job_state.append
    workdir = ["workdir"]; workdir_append = workdir.append
    exec_host = ["host"]; exec_host_append = exec_host.append
    iteration = ["num_iter"]; iteration_append = iteration.append
    maxforce = ["maxF"]; maxforce_append = maxforce.append
    force_atom = ["Atom"]; force_atom_append = force_atom.append
    neb_Ea = ["Ea"]; neb_Ea_append = neb_Ea.append
    slot = ["nn"]; slot_append = slot.append
    
    num = 0
    while num < len(qstatlines):
        if qstatlines[num].find("Job Id:") >= 0 and num == 0:
            line = qstatlines[num].replace("\n","").split(" ")
            while line.count("") > 0: line.remove("")
            JobID_append(str(line[2]))
            num += 1
    
        elif qstatlines[num].find("Job Id:") >= 0 and num > 1:
            if len(JobName) is not len(JobID): JobName_append("------")
            if len(JobOwner) is not len(JobID): JobOwner_append("------")
            if len(exec_host) is not len(JobID): exec_host_append("---")
            if len(walltime) is not len(JobID): walltime_append("00:00:00")
            if len(job_state) is not len(JobID): job_state_append("------")
            if len(workdir) is not len(JobID): workdir_append("------")    
            if len(slot) is not len(JobID): slot_append("-")

            line = qstatlines[num].replace("\n","").split(" ")
            while line.count("") > 0: line.remove("")
            JobID_append(str(line[2]))
            num += 1
     
        else:
            if qstatlines[num].find("Job_Owner") >= 0:
                line = qstatlines[num].replace("\n","").replace("@accel", "").split(" ")
                while line.count("") > 0: line.remove("")
                JobOwner_append(str(line[2]))
            if qstatlines[num].find("Job_Name") >= 0:
                line = qstatlines[num].replace("\n","").split(" ")
                while line.count("") > 0: line.remove("")
                JobName_append(str(line[2]))
            if qstatlines[num].find("exec_host") >= 0:
                line = qstatlines[num].replace("\n","").split(" ")
                while line.count("") > 0: line.remove("")
                exec_host_append(str(str(line[2]).split("/")[0].replace(nodename,"")))
            if qstatlines[num].find("resources_used.walltime") >= 0:
                line = qstatlines[num].replace("\n","").split(" ")
                while line.count("") > 0: line.remove("")
                walltime_append(str(line[2]))
            if qstatlines[num].find("job_state") >= 0:
                line = qstatlines[num].replace("\n","").split(" ")
                while line.count("") > 0: line.remove("")
                value = str(line[2])
                job_state_append(value)
            if qstatlines[num].find("Resource_List.nodect") >= 0:
                line = qstatlines[num].replace("\n","").split(" ")
                while line.count("") > 0: line.remove("")
                slot_append(str(line[2]))
            if qstatlines[num].find("PBS_O_WORKDIR") >= 0:
                if qstatlines[num +1].find("comment") >= 0:
                    dir1 = qstatlines[num].replace("\n","").replace("\t","").replace("PBS_O_WORKDIR=","").replace(",PBS_O_QUEUE=default","").replace(",", "")
                    while dir1.count(" ") > 0: dir1.remove(" ")
                    dir2 = dir1.replace(path_home,"~")
                    workdir_append(str(dir2))
                else:
                    line1 = qstatlines[num].replace("\n","").replace("\t","").replace("PBS_O_WORKDIR=","").replace(",","")
                    line2 = qstatlines[num +1].replace("\n","").replace("\t","").replace(",PBS_O_QUEUE=default","").replace(",","")
                    dir1 = line1 + line2
                    dir2 = dir1.replace(path_home,"~")
                    workdir_append(str(dir2))
                
                std_in_dir = [x for x in os.listdir(dir1) if x in "std.out"]
                vauto_in_dir = [x for x in os.listdir(dir1) if x in "vauto-input"]

                num_iter = "1N0"; last_f = "-----"; Ea = "----"; f_atom = "---"
                if len(vauto_in_dir) > 0:
                    path_INCAR = dir1 + "/calc/INCAR"
                    path_vauto = dir1 + "/vauto-input"

                    with open(path_INCAR) as fi: incarlines = fi.readlines()
                    NSW = [x.replace("NSW = ","").replace("\n","") for x in incarlines if "NSW = " in x][0]

                    with open(path_vauto) as fv: vautolines = fv.readlines()
                    redirs = [conv_line(x) for x in vautolines if len(conv_line(x)) > 0 and [v for v in conv_line(x) if v != "#"][0] in ["M","N","A"] and conv_line(x)[1] != "A"]
                    redirs = [[x for x in list if x != "#"] for list in redirs]
                   
                    num_d = 1
                    num_iter ="1N:0/" + NSW
                    for x in redirs:
                        if str(x[8])=="F": dirname="Prec"+str(x[0])+"cut"+str(x[1])+"k"+str(x[2])+"x"+str(x[3])+"x"+str(x[4])+"Ed"+str(x[7])
                        elif str(x[8])=="T": dirname="Prec"+str(x[0])+"cut"+str(x[1])+"k"+str(x[2])+"x"+str(x[3])+"x"+str(x[4])+"Ed"+str(x[7])+"Fd"+str(x[9])
                        
                        path_re_in_dir1 = dir1 + "/" + dirname
                        path_stdout = path_re_in_dir1 + "/std.out"
                        if os.path.exists(path_stdout):
                            with open(path_stdout) as fs: stdline = fs.read()
                            if flag_ra in stdline :
                                num_iter = str(num_d) + "R:" + str(stdline.count("F=")) + "/" + NSW
                                os.chdir(path_re_in_dir1); last_f,f_atom = last_force(path_re_in_dir1); os.chdir(path)
                            elif flag_wa in stdline: 
                                num_iter = str(num_d) + "W:" + str(stdline.count("F=")) + "/" + NSW
                                os.chdir(path_re_in_dir1); last_f,f_atom = last_force(path_re_in_dir1); os.chdir(path)
                            elif "F=" in stdline: 
                                num_iter = str(num_d) + "N:" + str(stdline.count("F=")) + "/" + NSW
                                os.chdir(path_re_in_dir1); last_f,f_atom = last_force(path_re_in_dir1); os.chdir(path)
                                break
                            else: num_iter = str(num_d) + "N:0"  + "/" + NSW; break
                        else: break
                        num_d += 1
                
                elif len(std_in_dir) > 0:
                    path_stdout = dir1 + "/std.out"
                    path_INCAR = dir1 + "/INCAR"
                    img_dirs = [x for x in os.listdir(dir1) if os.path.isdir(dir1+"/"+x) and len(x) == 2]

                    with open(path_INCAR) as fi: incarlines = fi.readlines()
                    NSW = [x.replace("NSW = ","").replace("\n","") for x in incarlines if "NSW = " in x][0]
                    
                    num_iter ="1N:0/" + NSW
                    if os.path.exists(path_stdout):
                        with open(path_stdout) as fs: stdline = fs.read()
                        if flag_ra in stdline : num_iter = "1R:" + str(stdline.count("F=")) + "/" + NSW
                        elif flag_wa in stdline: num_iter = "1W:" + str(stdline.count("F=")) + "/" + NSW
                        elif "F=" in stdline: num_iter = "1N:" + str(stdline.count("F=")) + "/" + NSW
                        else: num_iter = "1N:0/" + NSW 

                    if len(img_dirs) > 5:
                        os.chdir(dir1)
                        sub.call("nebef.pl > force.dat",shell=True)
                        with open(dir1 + "/force.dat") as ff: nebflines = ff.readlines()
                        list_nebf = [float(conv_line(x)[1]) for x in nebflines]
                        list_E = [float(conv_line(x)[3]) for x in nebflines]
                        Ea = max(list_E); last_f = list_nebf[list_E.index(Ea)]; last_f = "%.2f" % last_f; f_atom = "---"
                        if Ea < -10 or Ea > 20: Ea = "----"
                        else: Ea = "%.2f" % Ea
                        os.chdir(path)

                iteration_append(num_iter); maxforce_append(last_f); neb_Ea_append(Ea); force_atom_append(f_atom)
            num += 1

    # END: loop of qstatlines()

    if len(JobName) is not len(JobID): JobName_append("------")
    if len(JobOwner) is not len(JobID): JobOwner_append("------")
    if len(walltime) is not len(JobID): walltime_append("00:00:00")
    if len(job_state) is not len(JobID): job_state_append("------")
    if len(workdir) is not len(JobID): workdir_append("------")
    if len(exec_host) is not len(JobID): exec_host_append("---")
    if len(iteration) is not len(JobID): iteration_append("------")
    if len(maxforce) is not len(JobID): maxforce_append("----")
    if len(neb_Ea) is not len(JobID): neb_Ea_append("------")
    if len(force_atom) is not len(JobID): force_atom_append("--")

    """
    JobID = adjust_len(JobID)
    JobName = adjust_len(JobName)
    exec_host = adjust_len(exec_host)
    workdir = adjust_len(workdir)
    JobOwner = adjust_len(JobOwner)
    walltime = adjust_len(walltime)
    job_state = adjust_len(job_state)
    iteration = adjust_len(iteration)
    maxforce = adjust_len(maxforce)
    neb_Ea = adjust_len(neb_Ea)
    slot = adjust_len(slot)
 
    list_for_loop = [JobID, job_state, JobOwner, exec_host, slot, JobName, workdir, walltime, iteration, maxforce]
    if len(argv) > 1 and argv[1] == "-ea": list_for_loop.append(neb_Ea)

    L = "\n"; i = 0
    for line in JobID:
        for x in list_for_loop: L += str(x[i]) + " "
        else: L+= "\n"
        i += 1
    
    if len(argv) == 1 or (len(argv) > 1 and argv[1] == "-ea"): return L
    """

    list_for_loop =  [JobID, job_state, JobOwner, exec_host, slot, JobName, workdir, walltime, iteration, maxforce, force_atom]
    if len(argv) > 1 and argv[1] == "-ea": list_for_loop.append(neb_Ea)
    list_for_loop = [adjust_len(x) for x in list_for_loop]

    L = "\n"; i = 0
    for i1 in range(0,len(list_for_loop[0])):
        L += " " + " ".join([str(list_for_loop[i2][i1]) for i2 in range(0,len(list_for_loop))]) + "\n"

    if len(argv) == 1 or (len(argv) > 1 and argv[1] == "-ea"): return L, list_for_loop
# END: definition of function

if __name__ == "__main__": 
    L = qsdir()[0]
    print(L)
    path_qstat = "/home/nakao/record/qstat.out"
    with open(path_qstat, "w") as fq: fq.write(L+"\n")

# END
