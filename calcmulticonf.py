#! /usr/bin/env python3                                                                                   
# -*- coding: utf-8 -*-  

import os, sys, shutil, threading, glob, re
import subprocess as sub
import numpy as np
import fractions as fra
from functools import reduce

qsub = "qsub job.sh"

def conv_line(line):
    line = line.replace('\n',' ').replace('\r',' ').replace('\t',' ').replace('^M',' ')
    line = line.split(" ")
    while line.count("") > 0:
        line.remove("")
    return line

argv = sys.argv
path = os.getcwd()

path_tmp = path + "/tmp"
if os.path.exists(path_tmp) == False: print("\nYou should prepare tmp directory here! BYE!\n"); sys.exit()
path_models = path + "/models"
if os.path.exists(path_tmp) == False: print("\nYou should prepare models directory for putting POSCARs in that here! BYE!\n"); sys.exit()
poss_in_dir = sorted([x for x in os.listdir(path_models)])

if len(argv) < 2:
    print("\nPlease input the command of this shell after script name. if you input -h, you can see the kind of commands. BYE!\n")
    sys.exit()

if argv[1] == "-h":
    print("\n")
    print("-h: show help.")
    print("-mk: make the directories for calc..")
    print("-q: submit a job in the directories.")
    print("\n")
    sys.exit()

if argv[1] == "-mk":
    for pos in poss_in_dir:
        name = pos.replace(".vasp","")
        path_pos = path_models + "/" + pos
        path_dir = path + "/" + name
        path_calc = path_dir + "/calc"
        path_vinput = path_dir + "/vauto-input"
    
        shutil.copytree(path_tmp, path + "/" + name)
        shutil.copy(path_pos, path_calc)
    
        os.chdir(path_calc)
        sub.call("f2cc2f.py "+ pos + " p p",shell=True)    
        os.chdir(path_dir)
    
        with open(path_vinput) as fv: vautolines = fv.readlines()
        Lv = ""
        for line in vautolines:
            if "job_name:" in line: Lv += "job_name: " + name + "\n"
            else: Lv += line
        with open(path_vinput, "w") as fv: fv.write(Lv)
        sub.call("mkdfmr.py",shell=True)
        print(name + " has been made!")

elif argv[1] == "-q":
    for x in poss_in_dir:
        print(x.replace(".vasp",""))
        os.chdir(path + "/" + x.replace(".vasp",""))
        sub.call(qsub ,shell=True)

print("\nDone!\n")
