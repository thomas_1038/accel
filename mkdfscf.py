#! /usr/bin/env python3
# -*- coding: utf-8 -*-
#make .sh file

import sys, os, shutil
import subprocess as sub
#from pnnl import mkpot as mp

script = "mkdovasp.py"
#vasptype = 54
#numnode = 4
#hour = "3:00:00"
jobname = "scf"
ldos_eint_l = -1.0
ldos_eint_h = 0.0

#mkjob = str(script) + " --" + str(vasptype) + " --nn " + str(numnode) + " --n " + str(jobname) + " --t " + hour + " --p s"
mkjob = str(script) + " -j " + str(jobname)
needfiles = ["INCAR","KPOINTS","POTCAR","CONTCAR","WAVECAR","CHGCAR"]
dict_tag = {"LWAVE =":"LWAVE = T\n",
            "LCHARG =":"LCHARG = T\n",
            "ISTART = 0":"ICHARG = 0; ISTART = 2\n",
            "IBRION =":"IBRION = -1\n",
            "NSW =":"NSW = 0\n",
            "NELM =":"NELM = 1000\n",
            "NELMIN =":"NELMIN = 6\n",
            "NEDOS =":"NEDOS = 3001\n",
            "LVTOT =":"LVTOT = T\n",
            "LAECHG =":"LAECHG = T\n"}
Ljobadd = "mkldos.py -f\ncd LDOS-from" + str(ldos_eint_l) + "to" + str(ldos_eint_h) + "\nmpirun -n $process $path_vasp$vasp_type > std.out\n"

def conv_line(line):
    line = line.replace('\n',' ').replace('\r',' ').replace('\t',' ').replace('^M',' ')
    line = line.split(" ")
    while line.count("") > 0: line.remove("")
    return line

path = os.getcwd()
argv = sys.argv

if len(argv) == 1:
    direc_ref = input("Please input the directory name you want to use files in: ")
    lvtot = input("Do you calculate LOCPOT? [y/N]: ")
    if lvtot in ["y", "Y"]: f_lvtot = "T"
    elif lvtot in["n", "N"]: f_lvtot= "N"
    laechg = input("Do you calculate all electron Bader charge? [y/N]: ")
    if laechg in["y", "Y"]: f_laechg = "T"
    elif laechg in["n", "N"]: f_laechg = "N"
    
else:
    if str(argv[1]) in ["--h" or "-help"]:
        print("\n--h or -help: show HELP\n--rf: input reference directory\n--lp: LVTOT = T\n--bd: LAECHG = T\n"); sys.exit()
    if "--rf" in argv: index_r = argv.index("--rf"); direc_ref = argv[index_r +1]

    # LVTOT
    if "--lp" in argv: f_lvtot= "T"
    else: f_lvtot= "N"

    # LAECHG
    if "--bd" in argv: f_laechg = "T"
    else: f_laechg = "N"

# Path
path_ref = path + "/" + direc_ref
path_scf = path + "/SCF_" + direc_ref
os.mkdir(path_scf)

for f in needfiles:
    if os.path.exists(path_ref+"/"+f) == False:
        if f == "CONTCAR":  print("CONTCAR does not exist. So POSCAR is copied instead CONTCAR!"); f = "POSCAR"
        else: print(f + " does not exist.You should prepare " + f +"!"); continue

    if f == "CONTCAR": shutil.copy(path_ref+"/"+f, path_scf+"/POSCAR")
    elif f == "INCAR":
        with open(path_ref+"/"+f) as fi: incarlines = fi.readlines()

        L = ""
        for line in incarlines:
            for key in dict_tag:
                if key == "LVTOT =" and key in line:
                    if f_lvtot == "T": L += str(dict_tag[key]); break
                elif key == "LAECHG =" and key in line:
                    if f_laechg == "T": L += str(dict_tag[key]); break
                elif key in line: L += str(dict_tag[key]); break
            else: L += line
        with open(path_scf+"/INCAR","w") as fi: fi.write(L)

    else: shutil.copy(path_ref+"/"+f, path_scf)

# Make dovasp
os.chdir(path_scf)
if os.path.exists(path_scf+"/POTCAR") == False: mp.mk_pot()
sub.call(mkjob, shell=True)
with open(path_scf + "/job.sh","a") as fj: fj.write(Ljobadd)

print("\nDone!\n")
# END: Program
