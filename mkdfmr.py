#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os,shutil
from stat import *
import subprocess as sub

############ START: PLEASE INPUT ################
inputfile = "vauto-input"
path_vasp="/home/nakao/vasp/bin/"
#vasp_type="vasp541_std"
num_core = 16
mkpot = "mkpot.py"
#path_qsub = "/apps/t3/sles12sp2/uge/latest/bin/lx-amd64/qsub"
############ END: PLEASE INPUT ##################

def conv_line(line):
    line = line.replace('\n',' ').replace('\r',' ').replace('\t',' ').replace('^M',' ').split(" ")
    while line.count("") > 0: line.remove("")
    return line

def mkarray(dirs_l,j):
    if j == 0: L =  "array=("
    else: L =  "#array=("
    num = 0
    for dir_prec_l in dirs_l:
        if num == int(len(dirs_l) -1):
            L += " \"" + dir_prec_l + "\")\n"
            break
        L += "\"" + dir_prec_l + "\" "
        num += 1
    return L

def mk_kp(k1, k2, k3):
    # make KPOINTS
    Lkp = "Automatic mesh\n0\n"
    if kp_method == "G": Lkp += "Gamma\n"
    else: Lkp += "Monkhorst-pack\n"
    Lkp += k1 + " " + k2 + " " + k3 + "\n"
    Lkp += "0 0 0\n"
    return Lkp

def mk_dovasp(num_node, num_core, job_name, path_vasp, vasp_type, dirs, dirs2):
    # make job.sh
    Lsh  = "#!/bin/sh\n"
    Lsh += "#PBS -l nodes=" + str(num_node) + ":ppn=" + str(num_core) + "\n"
    Lsh += "#PBS -N " + job_name + "\n"
    Lsh += "cd $PBS_O_WORKDIR\n"
    Lsh += "NPROCS=`wc -l <$PBS_NODEFILE`\n"
    Lsh += "export MKL_NUM_THREADS=1\n\n"

    Lsh += "path_vasp=\"" + str(path_vasp) + "\"\n"
    Lsh += "vasp_type=\"" + str(vasp_type) + "\"\n"
#    Lsh += "process=" + str(int(num_node)*num_core) +  "\n\n"
    Lsh += "process=" + str(num_core) + "\n\n"

    num_d = 0
    for direc_prec in dirs2:
        Lsh += mkarray(dirs,num_d)
        del dirs[0]
        num_d += 1

    Lsh += "for ((i = 0; i < ${#array[@]}; i++))\n"
    Lsh += "do\n"
    Lsh += "    cd ./${array[$i]}\n"
    #Lsh += "    cp INCAR_act INCAR\n"
    Lsh += "    date >> date.txt\n"
    Lsh += "    mpirun -n $process $path_vasp$vasp_type > std.out\n\n"
    Lsh += "    while read line\n"
    Lsh += "    do\n"
    Lsh += "        if [[ `echo $line|grep 'reached required accuracy'` ]]; then\n"
    Lsh += "            break\n"
    Lsh += "        elif  [[ `echo $line|grep 'BAD TERMINATION OF ONE OF YOUR APPLICATION PROCESSES'` ]]; then\n"
    Lsh += "            date >> date.txt\n"
    Lsh += "            exit 1\n"
    Lsh += "        elif  [[ `echo $line|grep 'soft stop encountered!  aborting job ...'` ]]; then\n"
    Lsh += "            date >> date.txt\n"
    Lsh += "            exit 1\n"
    Lsh += "        fi\n"
    Lsh += "    done < ./std.out\n"
    Lsh += "    date >> date.txt\n\n"
    Lsh += "    I=$((${#array[*]}-1))\n"
    Lsh += "    if [ $i = $I ];then\n"
    Lsh += "        echo \"break\"\n"
    Lsh += "        break\n"
    Lsh += "    fi\n\n"
    Lsh += "    j=$(($i+1))\n"
    Lsh += "    cp CONTCAR ../${array[$j]}/POSCAR\n"
    Lsh += "    cp WAVECAR ../${array[$j]}/WAVECAR\n"
    Lsh += "    cp CHGCAR ../${array[$j]}/CHGCAR\n\n"
    Lsh += "    cd ../\n"
    Lsh += "done\n"
    Lsh += "date >> date.txt\n"
    return Lsh

# Definition of path
path = os.getcwd()
path_input = path + "/" + inputfile

# Read input file
with open(path_input) as fi: inputlines = fi.readlines()

ilist = [conv_line(line) for line in inputlines if len(conv_line(line)) > 0]
del ilist[0]; del ilist[-1]

num_t = 0; num_c = 10000; clist = []
for tag in ilist:
    if "calc_set:" in tag: calcset = str(tag[1])
    elif "job_name:" in tag: job_name = str(tag[1])
    elif "kp_method:" in tag: kp_method = str(tag[1])
    elif "cp_wc:" in tag: cp_wc = str(tag[1])
    elif "vasp_type:" in tag: vasp_type = str(tag[1])
    elif "node_type:" in tag: node_type = str(tag[1])
    elif "num_node:" in tag: num_node = str(tag[1])
    elif "process:" in tag: process = str(tag[1])
    elif "NPAR:" in tag: NPAR = str(tag[1])
    elif "KPAR:" in tag: KPAR = str(tag[1])
    elif "job_time:" in tag: job_time = str(tag[1])
    elif "Prec" in tag: num_c = num_t

    if num_t > num_c and tag[0] != "#": clist.append(tag)
    num_t += 1

path_calcset = path + "/" + calcset
POSCAR_ini = path + "/" + calcset + "/POSCAR"
POTCAR_ini = path + "/"+ calcset + "/POTCAR"
INCAR_ini = path + "/" + calcset + "/INCAR"
KPOINTS_ini = path + "/" + calcset + "/KPOINTS"
WAVECAR_ini = path + "/" + calcset + "/WAVECAR"
CHGCAR_ini = path + "/" + calcset + "/CHGCAR"
os.chdir(path_calcset); sub.call(mkpot, shell = True); os.chdir(path)

### Start: make the directories ###
dirs =[]; dirs1 = []; dirs2 = []
for num_prec, name in enumerate(clist):
    prec = str(name[0])
    encut = str(name[1])
    k1 = str(name[2])
    k2 = str(name[3])
    k3 = str(name[4])
    ediff = str(name[7])
    fdiff = str(name[9])
    ibrion =  str(name[5])
    nelmin = str(name[6])

    if str(name[8]) == "F": dirname = "Prec" + prec + "cut" + encut + "k" + k1 + "x"+ k2+ "x" + k3 + "Ed" + ediff
    elif str(name[8]) == "T": dirname = "Prec" + prec + "cut" + encut + "k" + k1 + "x"+ k2 + "x" + k3 + "Ed" + ediff + "Fd" + fdiff

    print(dirname)
    path_dir = path + "/" + dirname
    os.mkdir(dirname); dirs.append(dirname); dirs1.append(dirname); dirs2.append(dirname)

    # copy INCAR, KPOINTS and POTCAR in calcset
    shutil.copy(POTCAR_ini, path_dir)
    if num_prec == 0: shutil.copy(POSCAR_ini, path_dir)

    # make KPOINTS
    with open(path_dir + "/KPOINTS","w") as fkp: fkp.write(mk_kp(k1, k2, k3))

    # read INCAR
    with open(INCAR_ini) as fin: incarlines = fin.readlines()

    # add EDIFF & EDIFFG to INCAR
    i = 0
    while i < len(incarlines):
        if incarlines[i].find("SYSTEM =") >= 0: incarlines[i] = "SYSTEM = " + job_name + ", cutoff:" + encut + "eV" + " k:" + k1 + "x" + k2 + "x" + k3 + "\n"
        elif incarlines[i].find("PREC =") >= 0 and incarlines[i].find("SYMPREC =") < 0: incarlines[i] = "PREC = " + prec + "\n"
        elif incarlines[i].find("ENCUT =") >= 0:  incarlines[i] = "ENCUT = " + encut + "\n"
        elif incarlines[i].find("IBRION =") >= 0: incarlines[i] = "IBRION = " + ibrion + "\n"
        elif incarlines[i].find("NELMIN =") >= 0: incarlines[i] = "NELMIN = " + nelmin + "\n"
        #elif incarlines[i].find("NPAR = ") >= 0: incarlines[i] = "NPAR = " + str(NPAR) + "\n"
        #elif incarlines[i].find("KPAR = ") >= 0: incarlines[i] = "KPAR = " + str(KPAR) + "\n"
        elif incarlines[i].find("EDIFF =") >= 0:  incarlines[i] = "EDIFF = " + ediff + "\n"
        elif incarlines[i].find("EDIFFG =") >= 0:
            if str(name[8]) == "F": incarlines[i] = "!EDIFFG = " + "\n"
            elif str(name[8]) == "T": incarlines[i] = "EDIFFG = -" + fdiff + "\n"
        elif incarlines[i].find("ISTART = 0") >= 0 and num_prec > 0:
             incarlines[i] = "!" + incarlines[i]
        elif incarlines[i].find("ICHARG = 1 ; ISTART = 1") >= 0 and num_prec > 0:
             incarlines[i] ="ISTART = 1 ; ICHARG = 1\n"
        elif incarlines[i].find("ISTART = 1 ; ICHARG = 1") >= 0 and num_prec > 0:
             incarlines[i] ="ISTART = 1 ; ICHARG = 1\n"
        i += 1

    with open(path_dir + "/INCAR",'w') as fin: fin.write("".join(incarlines))
# END: make directories #

path_job = path + "/job.sh"
with open(path_job,"w") as fj: fj.write(mk_dovasp(num_node, num_core, job_name, path_vasp, vasp_type, dirs, dirs2))
os.chmod(path_job, S_IRUSR | S_IWUSR | S_IXUSR)

# END
