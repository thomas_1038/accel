#! /usr/bin/env python3
# -*- coding: utf-8 -*-

#2015/05/01 ver1.2
#2016/06/19 ver1.3
#make LDOS at optional level

import os, sys, shutil
import subprocess as sub
#from pnnl import mkpot as mp

script = "mkdovasp.py"
#vasptype = 54
#numnode = 1
#jobname = "ldos"
#hour = "0:30:00"
range_int_l = -1.0
range_int_h = 0.0
#partition = "p"
#mkdv = script + " --" + str(vasptype) + " --nn " + str(numnode) + " --n " + str(jobname) + " --t " + hour + " --p " + str(partition)
mkdv = script + " -j ldos"
needfiles = ["CONTCAR","KPOINTS","INCAR","POTCAR","WAVECAR"]

argv = sys.argv

if len(argv) == 1:
    range_EINT_l = input("Please input the lower energy value, for example -1.2: ")
    range_EINT_h = input("Please input the higher energy value, for example 0.0: ")
elif argv[1] == "-f": range_EINT_l = range_int_l; range_EINT_h = range_int_h

name = "LDOS-" + "from" + str(range_EINT_l) + "to" + str(range_EINT_h)

dict_incar = {"!LPARD = ":"LPARD = T\n",
              "!LWAVE = ":"LWAVE = F\n",
              "LWAVE = ":"LWAVE = F\n",
              "!NBMOD = ":"NBMOD = -3\n",
              "!EINT = ":"EINT = " + str(range_EINT_l) + " " + str(range_EINT_h) + "\n",
              "IBRION = ":"IBRION = -1\n",
              "NSW = ":"NSW = 0\n",
              "ISTART = 0 ; ICHARG = 2":"ISTART = 1\n",
              "ICHARG = 1 ; ISTART = 1":"ISTART = 1\n",
              "LVTOT = ":"!LVTOT = \n",
              "NEDOS = ":"!NEDOS = \n"}

path = os.getcwd()
path_dir = path + "/" + name
os.mkdir(path_dir)

#copy files to LDOS
for f in needfiles:
    if os.path.exists(path + "/" + f) == False:
        if f == "WAVECAE" or f == "INCAR": print(f + " doesn't exist! You should prepare it! BYE!"); sys.exit()
        elif f == "CONTCAR": print(f + " doesn't exist! POSCAR is copied instead CONTCAR."); f = "POSCAR"
        else: print(f + " doesn't exist!"); continue

    if f == "CONTCAR": shutil.copy(path+"/"+f, path_dir+"/POSCAR")
    elif f == "WAVECAR": shutil.copy(path+"/"+f, path_dir)
    elif f == "INCAR":
        with open(path + "/" + f) as fi: incarlines = fi.readlines()

        L = ""
        for line in incarlines:
            for key in dict_incar:
                if key in line: L += dict_incar[key]; break
            else: L += line
        with open(path_dir+"/INCAR","w") as fi: fi.write(L)
    else: shutil.copy(path+"/"+f, path_dir)

#shfiles = [x for x in os.listdir(path) if os.path.isfile(path+"/"+x) and ".sh" in x]
#if len(shfiles) > 0: shutil.copy(path + "/" + str(shfiles[0]), path_dir)
os.chdir(path_dir)
if os.path.exists(path_dir+"/POTCAR") == False: sub.call("mkpot.py", shell=True)
sub.call(mkdv, shell=True)
    
print("\n"+ name + " has been made!\n")
#END: Program
