#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#usage: script

import os, sys, shutil, mkpot, time
import subprocess
import make.mkkplist as mkkplist
import chg.chgincarnks as cin
import mkdovasp as mkdv

def calc_cutkp(nowpath, nn=1, list_cutoff=[500]):
    def conv_line(line):
        line = line.replace('\n',' ').replace('\r',' ').replace('\t',' ').replace('^M',' ').split(" ")
        while line.count("") > 0:line.remove("")
        return line

    # Read cif file
    ciffiles = sorted([x for x in os.listdir(nowpath) if ".cif" in x])
    for cif in ciffiles:
        path_calc_dir = nowpath + "/" + cif.replace("_conventional_standard.cif","")
        if not os.path.exists(path_calc_dir): os.mkdir(path_calc_dir)

        path_cutkp1p = path_calc_dir + "/cutkp1p"
        if not os.path.exists(path_cutkp1p): os.mkdir(path_cutkp1p)

        shutil.copy(nowpath + "/" + cif, path_cutkp1p)
        path_cif_in_dir = path_cutkp1p + "/" + cif

        if not os.path.exists(path_cif_in_dir): print("You should prepare the cif-file! BYE!")
        with open(path_cif_in_dir) as fc: ciflines = fc.readlines()
        jobname = ["".join(conv_line(x.replace("_chemical_formula_structural","").replace("'",""))) for x in ciflines if "_chemical_formula_structural" in x][0]

        cif2pos = 'cif2cell -p vasp --vasp-format=5 --vasp-encutfac=1.0 --vasp-pseudo-priority=\"_d,_pv,_sv,_h,_s\" --vasp-print-species ' +\
                  '--vasp-cartesian-lattice-vectors -f '+ path_cif_in_dir + ' --no-reduce'
        os.chdir(path_cutkp1p)
        subprocess.call(cif2pos, shell=True)

        path_calc = path_cutkp1p + "/calc"
        if not os.path.exists(path_calc): os.mkdir(path_calc)

        # copy POSCAR in calc
        #time.sleep(1)
        shutil.copy(path_cutkp1p + "/POSCAR", path_calc)

        # make kplist
        kplist = mkkplist.mk_kplist(path_calc, "F")[1]

        # make POTCAR in calc
        mkpot.mk_pot(path_calc)

        # Convert INCAR for NPAR and KPAR
        os.chdir(path_calc)
        subprocess.call("mkincar.py -ck1", shell=True)
        cin.conv_INCAR(path_calc, "INCAR", 4, 1, 24)
        #subprocess.call("chgincarnks.py", shell=True)

        # Definition
        path_pos_bf = path_calc + "/POSCAR"
        path_pot_bf = path_calc + "/POTCAR"
        path_inc_bf = path_calc + "/INCAR"

        for num_c, encut in enumerate(list_cutoff):
            for num_k, l in enumerate(kplist):
                kp = l[0]
                direcname = "cut" + str(encut) + "k" + str(kp[0]) + "x" + str(kp[1]) + "x" + str(kp[2])
                path_dir = path_cutkp1p + "/" + str(direcname)
                os.mkdir(str(path_dir))

                #difinition
                path_pos_af = path_dir + "/POSCAR"
                path_pot_af = path_dir + "/POTCAR"
                path_inc_af = path_dir + "/INCAR"
                #sh_after = path_dir + "/test.sh"

                #copy POSCAR, POTCAR, INCAR
                shutil.copy(path_pos_bf, path_pos_af)
                shutil.copy(path_pot_bf, path_pot_af)
                #shutil.copy(sh_before,sh_after)

                #make KPOINTS
                os.chdir(path_dir)
                subprocess.call("mkkp.py " + str(kp[0]) + " " + str(kp[1]) + " " + str(kp[2]) + " m", shell=True)

                #make .sh
                job_name = str(jobname) + "-c" + str(encut) + "k" + str(kp[0]) + str(kp[1]) + str(kp[2])

                L = mkdv.make_dovasp("accel", job_name)
                path_job = path_dir + "/job.sh"
                with open(path_job, "w") as fj: fj.write(L)
                os.chmod(path_job, 0o755)

                #make INCAR
                with open(path_inc_bf) as fi: incarlines = fi.readlines()

                num = 0
                while num < len(incarlines):
                    if incarlines[num].find("ENCUT =") >= 0: incarlines[num] = "ENCUT = " + str(encut) + "\n"
                    num += 1
                with open(path_inc_af,'w') as fi: fi.write("".join(incarlines))

                subprocess.call("qsub job.sh", shell=True)

    # make ciffiles (directory)
    path_cifs = nowpath + "/cifs"
    if not os.path.exists(path_cifs):
        os.mkdir(path_cifs)
    for cif in ciffiles: shutil.move(nowpath + "/" + cif, path_cifs)

if __name__ == "__main__":
    calc_cutkp(os.getcwd())

    """
    argv = sys.argv
    if len(argv) > 1:
        if str(argv[1]) == "-h":
            print("\n")
            print("-h: help")
            print("-nn: the number of node")
            print("\n")
            sys.exit()

        if "-nn" in argv: index_nn = argv.index("-nn"); nn = str(argv[index_nn +1])
        else: nn = input("Please input the number of nodes, e.g., 1: ")

    elif len(argv) == 1:
        nn = input("Please input the number of nodes, e.g., 1: ")
    """
# END