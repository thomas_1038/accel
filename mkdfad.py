#!/usr/bin/env python3
# -*- coding: utf-8 -*-  
# 2018/06/11: change around mkpot.py

import os
import sys
import shutil
import threading
import glob
import subprocess as sub
#import numpy as np

### Definition of functions ###
def conv_line(line):
    line = line.replace('\n',' ')
    line = line.replace('\r',' ')
    line = line.replace('\t',' ')
    line = line.replace('^M',' ')
    line = line.split(" ")
    while line.count("") > 0:
        line.remove("")
    return line
### End ###

######################### INPUT ZONE #########################

adsites = ["oe", "be", "he", "osi", "osf", "bsi", "bsf"]
path_ilist = "/home/nakao/script/tn_scripts_accel/vauto-input-forsurf"

######################### INPUT ZONE #########################

print("\nYou Should Check Selective Dynamics TAG in POSCAR!")
Q_sd = input("Please input OK or NOT OK [o, n]: ")
if Q_sd in ["o", "OK", "ok"]:
    pass
else:
    "\nYou should change Selective Dynamics TAG in POSCAR! BYE!\n"
    sys.exit()

path = os.getcwd()

# make POSCAR-list
poscars = [x for x in os.listdir(path) if "vasp" in x]
poscars.sort()

# read POSCAR01 & print Element labels
#path_pos_ini = path + "/" + str(poscars[0])
#fpos = open(path_pos_ini)
#poslines = fpos.readlines()
#fpos.close()
# print "\nElements in POSCAR"
# print  str(poslines[5])

### make template-dierctory ###
def mktmpandcalc():
    path_tmp = path + "/tmp"
    path_calc = path + "/tmp/calc"
    os.mkdir(path_tmp)
    os.mkdir(path_calc)

    # copy POSCAR
#    path_pos_in_calc = path_calc + "/POSCAR"
#    shutil.copy(path_pos_ini, path_pos_in_calc)

    os.chdir(path_calc)
    # make POTCAR
#    pot = raw_input("Please input the name of POTCAR, e.g., Ca_sv N H: ")
#    mkpot = "mkpot.py" # + pot
#    sub.call(mkpot, shell=True)
 
    # make INCAR
    sub.call("mkincar.py",shell=True)

    # determine ENCUT
    print("\n")
    Q_lencut = input("Please input the \"LOW\" cut-off energy you want to use. e.g., 400 or default, d: ")
    if Q_lencut in ["d"]:
        pass
    else:
        lencut = Q_lencut

    Q_hencut = input("Please input the \"HIGH\" cut-off energy you want to use. e.g., 450 or default, d: ")
    if Q_hencut in ["d"]:
        pass
    else:
        hencut = Q_hencut
    print("\n")

    # make k-points list
    sub.call("mkkplist.py --s", shell=True)
    sub.call("cat kplist", shell=True)

    # make new vauto-input
    print("\n")
    kp_r = input("Please input the raugh k-points, e.g., 2 2 1: ")
    kp_f = input("Please input the raugh k-points, e.g., 4 4 1: ")
    print("\n")
    list_kpr = conv_line(kp_r)
    list_kpf = conv_line(kp_f)

    # read vauto-input
    fi = open(path_ilist)
    ilines = fi.readlines()
    fi.close()

    ilist = [conv_line(line) for line in ilines if len(conv_line(line)) > 0]
    del ilist[0]
    del ilist[-1]

    num_t = 0
    num_c = 10000
    clist = []
    for tag in ilist:
        if "calc_set:" in tag:
            calcset = str(tag[1])
        elif "job_name:" in tag:
            job_name = str(tag[1])
        elif "kp_method:" in tag:
            kp_method = str(tag[1])
        elif "cp_wc:" in tag:
            cp_wc = str(tag[1])
        elif "vasp_type:" in tag:
            vasp_type = str(tag[1])
#        elif "node_type:" in tag:
#            node_type = str(tag[1])
        elif "num_node:" in tag:
            num_node = str(tag[1])
#        elif "process:" in tag:
#            process = str(tag[1])
#        elif "NPAR:" in tag:
#            NPAR = str(tag[1])
#        elif "KPAR:" in tag:
#            KPAR = str(tag[1])
#        elif "job_time:" in tag:
#            job_time = str(tag[1])
        elif "Prec" in tag:
            num_c = num_t

        if num_t > num_c and tag[0] != "#":
            clist.append(tag)
            num_t += 1

    print("job_name: " + job_name)
    print("vasp_type = " + vasp_type)
#    print "node_type = " + node_type
    print("num_node = " + num_node)
#    print "process = " + process
#    print "NPAR = " + NPAR
#    print "KPAR = " + KPAR
#    print "job_time = " + job_time
    print("kp_method = " + kp_method + "\n")

    Q_job = input("Please input the job name or default, d: ")
    if Q_job in ["d"]:
        pass
    else:
        job_name = Q_job

    Q_chg = input("Do you want to change above parameters? [y/N]: ")
    if Q_chg in ["n","N"]:
        pass
    elif Q_chg in ["y","Y"]:
        Q_vasptype = input("Please input the type of vasp, [vasp541_std,s / vasp541_ncl,nc / vasp541_gam,g / vasp541wan_std,w / vasp541neb_std,ne / default,d]: ")
        if Q_vasptype in ["d"]:
            pass
        elif Q_vasptype in ["s","std","vasp541_std"]:
            vasptype = "vasp541_std"
        elif Q_vasptype in ["nc","ncl","vasp541_ncl"]:
            vasptype = "vasp541_ncl"
        elif Q_vasptype in ["g","gam","vasp541_gam"]:
            vasptype = "vasp541_gam"
        elif Q_vasptype in ["w","wan","vasp541wan_std"]:
            vasptype = "vasp541wan_std"
        elif Q_vasptype in ["ne","neb","vasp541neb_std"]:
            vasptype = "vasp541neb_std"

#        Q_nodetype = raw_input("Please input the type of node, [default, d / f_node, fn / h_node, hn / q_node, qn / s_core, sc / q_core, qc / s_gpu, g ]: ")
#        if Q_nodetype in ["d"]:
#            pass
#        elif Q_nodetype in ["fn", "f_node"]:
#            nodetype = "f_node"
#        elif Q_nodetype in["hn", "h_node"]:
#            nodetype = "h_node"
#        elif Q_nodetype in["qn", "q_node"]:
#            nodetype = "q_node"
#        elif Q_nodetype in["sc", "s_core"]:
#            nodetype = "s_core"
#        elif Q_nodetype in["qc", "q_core"]:
#            nodetype = "q_core"
#        elif Q_nodetype in["g", "s_gpu"]:
#            nodetype = "s_gpu"

        Q_numnode = input("Please input the number of node you want to use. e.g., 1 or defalt, d: ")
        if Q_numnode in ["d"]:
            pass
        else:
            numnode = Q_numnode

#        Q_process = raw_input("Please input the number of process you want to use. e.g., 3 or default, d: ")
#        if Q_process in ["d"]:
#            pass
#        else:
#            process = Q_process

#        Q_NPAR = raw_input("Please input NPAR you want to use. e.g., 4 or default, d: ")
#        if Q_NPAR in ["d"]:
#            pass
#        else:
#            NPAR = Q_NPAR

#        Q_KPAR = raw_input("Please input KPAR you want to use. e.g., 1 or default, d: ")
#        if Q_KPAR in ["d"]:
#            pass
#        else:
#            KPAR = Q_KPAR

#        Q_time = raw_input("Please input the wall time you want to use. e.g., 1:00:00 or default, d: ")
#        if Q_time in ["d"]:
#            pass
#        else:
#            job_time = Q_time

        Q_kpmethod = input("Please input k-points method, Gamma center or Monkhorst-Pack, [g/m] or default, d: ")
        if Q_kpmethod in ["d"]:
            pass
        elif Q_kpmethod in ["g"]:
            kp_method = "G"
        elif Q_kpmethod in ["m"]:
            kp_method = "M"

    ilines_re = []
    ilines_re_append = ilines_re.append
    numJ_prec = 0
    for iline in ilines:
        if iline.find("job_name:") >= 0:
            iline = "job_name: " + job_name + "\n"
        elif iline.find("kp_method:") >= 0:
            iline = "kp_method: " + kp_method + "\n"
        elif iline.find("vasp_type:") >= 0:
            iline = "vasp_type: " + vasp_type + "\n"
#        elif iline.find("node_type:") >= 0:
#            iline = "node_type: " + node_type + "\n"
        elif iline.find("num_node:") >= 0:
            iline = "num_node: "+ num_node + "\n"
#        elif iline.find("process:") >= 0:
#            iline = "process: " + process + "\n"
#        elif iline.find("NPAR:") >= 0:
#            iline = "NPAR: " + NPAR + "\n"
#        elif iline.find("KPAR:") >= 0:
#            iline = "KPAR: " + KPAR + "\n"
#        elif iline.find("job_time:") >= 0:
#            iline = "job_time: " + job_time + "\n"
        elif "Prec" in iline:
            numJ_prec = 1
        elif "M" in iline and "e-" in iline and numJ_prec == 1:
            iline = "M    " + lencut + "  " + kp_r + "   2      2      1e-4  F  No\n"
        elif "N" in iline and "e-" in iline and numJ_prec == 1:
            iline = "N    " + hencut + "  " + kp_f + "   2      4      1e-5  F  No\n"
        elif "A" in iline and "1e-5" in iline and "#" in iline and numJ_prec == 1:
            iline = "# A    " + hencut + "  " + kp_f + "   2      6      1e-5  T  1e-2\n"
        elif "A" in iline and "1e-6" in iline and "#" in iline and numJ_prec == 1:
            iline = "# A    " + hencut + "  " + kp_f + "   2      6      1e-6  T  1e-2\n"
        elif "A" in iline and "1e-5" in iline and "#" not in iline and numJ_prec == 1:
            iline = "# A    " + hencut + "  " + kp_f + "   2      6      1e-5  T  1e-2\n"
        elif "A" in iline and "1e-6" in iline and "#" not in iline and numJ_prec == 1:
            iline = "# A    " + hencut + "  " + kp_f + "   2      6      1e-6  T  1e-2\n"
        ilines_re_append(iline)

    path_ilist_in_tmp = path_tmp + "/vauto-input"
    fi = open(path_ilist_in_tmp,"w")
    for line in ilines_re:
        fi.write(line)
    fi.close()
    os.chdir(path)

    Q_exit = input("If you want to exit this system, please enter [q, Q]: ")
    if Q_exit in ["q","Q"]:
        sys.exit()

### End ###

path_tmp = path + "/tmp"
flag_tmp = os.path.exists(path_tmp)
if flag_tmp == False:
    mktmpandcalc()
    print("\ntmp and calc have been made!\n")
else:
    print("\ntmp and calc already exist!\n")

path_input_in_tmp = path_tmp + "/vauto-input"
fi = open(path_input_in_tmp)
ilines = fi.readlines()
fi.close()

### make directories of adsites ###
for pos in poscars:
    L = pos.replace("POSCAR_","")
    L = L.replace(".vasp","")
    
    if "oe" in L:
        num = L.replace("oe","")
        dirname = "ontop_endon" + num
        L_forjob = "oe" + num
    elif "be"in L:
        num = L.replace("be","")
        dirname = "bridge_endon" + num
        L_forjob = "be" + num
    elif "he"in L:
        num = L.replace("he","")
        dirname = "hollow_endon" + num
        L_forjob = "he" + num
    elif "osi"in L:
        num = L.replace("osi","")
        dirname = "ontop_sideon_ini" + num
        L_forjob = "osi" + num
    elif "osf"in L:
        num = L.replace("osf","")
        dirname = "ontop_sideon_fin" + num
        L_forjob = "osf" + num
    elif "bsi"in L:
        num = L.replace("bsi","")
        dirname = "bridge_sideon_ini" + num
        L_forjob = "bsi" + num
    elif "bsf"in L:
        num = L.replace("bsf","")
        dirname = "bridge_sideon_fin" + num
        L_forjob = "bsf" + num

    path_dir = path + "/" + dirname
    path_pos = path + "/" + pos
    path_calc_in_dir = path_dir + "/calc"
    path_pos_in_calc_in_dir = path_calc_in_dir + "/POSCAR"
    print("\n"+ dirname)

    # make directory
    shutil.copytree(path_tmp, path_dir)
    shutil.copy(path_pos, path_pos_in_calc_in_dir)

    # make POTCAR
    os.chdir(path_calc_in_dir)
    sub.call("mkpot.py",shell=True)

    # make calcautorelax.py in each dir 
    ilines_new = []
    append_i = ilines_new.append
    for iline in ilines:
        if "job_name:" in iline:
            value = iline.replace("\n","") + "_" + str(L_forjob) + "\n"
            append_i(value)
        else:
            append_i(iline)
        
    path_input_in_dir = path_dir + "/vauto-input"
    f_i = open(path_input_in_dir, "w")
    for line in ilines_new:
        f_i.write(line)
    f_i.close()
    
    # make PREC-directories
    os.chdir(path_dir)
    sub.call("mkdfmr.py",shell=True)
    os.chdir(path)

### End ###

# make c-list
#path_clist = path + "/c-list"
#fc = open(path_clist,"w")
#for cline in L_forclist:
#    fc.write(cline)
#fc.close()

# make POSCARs(dir)
path_POSCARs = path + "/POSCARs"
flag_POSCARs = os.path.exists(path_POSCARs)
if flag_POSCARs == False:
    os.mkdir(path_POSCARs)

# move POSCARs
for pos in poscars:
    path_pos = path + "/" + pos
    shutil.move(path_pos, path_POSCARs)

print("\nDone!\n")



